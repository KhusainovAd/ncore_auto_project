//�������� � �������� vistibility Xpath
package consultant_test;

import assist.Constant;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait {
    public static void until(WebDriver driver, String Xpath) throws Exception {
        until(driver, Xpath, 0);
    }

    public static void until(WebDriver driver, String Xpath, int millis) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, Constant.TimeOutSec);
        if (millis == 0) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath)));
            Thread.sleep(50);
        } else {
            Thread.sleep(millis);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath)));
            Thread.sleep(50);
        }
    }
}
