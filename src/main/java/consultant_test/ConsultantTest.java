package consultant_test;

import assist.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ConsultantTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String SearchPhrase;

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        baseUrl = Constants.BaseUrl;
        SearchPhrase = Constants.SearchPhrase;

        driver = Constants.driver;
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(Constants.ImplicityWait, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void test1() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        // #1 ��������� ����� ���������� �� ����� "�� �2":
        Wait.until(driver, "//*[@id[contains(.,'dictFilter')]]");
        driver.findElement(By.xpath("//*[@id[contains(.,'dictFilter')]]")).sendKeys(SearchPhrase);
        Wait.until(driver, "//span[text()[contains(.,'�����')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'�����')]]")).click();

        // #2 ������� ������ �������� �� ������ ����������� ������
        Wait.until(driver, "//div[@class='listPaneContent']/div[@index='0']");
        driver.findElement(By.xpath("//div[@class='listPaneContent']/div[@index='0']")).click();

        // #3 ��������� ��� ����� �������� ��������� �� ������ ������� �������� ������ �������� �� ����� 10 ������
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        Thread.sleep(5000);
        //driver.findElement(By.xpath("//*[@id='mainContent']/div[1]/table/tbody/tr/td[4]/div/button[2]")).click();
        assertTrue(Timer.GetTime(driver, "//div[@class[contains(.,'textContainer')]]", 3000) < 10000);

        // #4 ��������� ��� �������� ���������� �������� �� ������� ������ "��������� ������" �  ������ "����� ������" ��� ����� ��������
        //� �������� ��������
        String ActualTitle;
        ActualTitle = driver.getTitle();
        ActualTitle = ActualTitle.replace("\""," ");
        assertTrue(ActualTitle.contains(Constants.text2));
       //assertTrue(ActualTitle.contains(Constants.text1));

        // #5 ��������� ��� � ������: "����� � ������" ��������� ��������� ����� "�� �2"
        Assert.assertEquals(SearchPhrase, driver.findElement(By.xpath("//*[@id[contains(.,'dictFilter')]]")).getAttribute("value"));

        // #6 ������� ����������, ��������� ����� �� ���������� "������ 163", ������� �� ���������� �� ��� ������� � ������ ���������
        Wait.until(driver,"//*[text()='����������']");
        driver.findElement(By.xpath("//*[text()='����������']")).click();
        Wait.until(driver,"//*[@id='findContext']");
        driver.findElement(By.xpath("//*[@id='findContext']")).clear();
        driver.findElement(By.xpath("//*[@id='findContext']")).sendKeys(Constants.Chapter, Keys.ENTER);
        Wait.until(driver,"//span[text()[contains(.,'"+Constants.Chapter+"')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'"+Constants.Chapter+"')]]")).click();

        int Counter = Integer.parseInt(Constants.Chapter);
        String NextChapter = Integer.toString(Integer.parseInt(Constants.Chapter)+1);
        // #7 �������� ���� ����� ��������� 163 ������. ������ ������.
        for (int i = 0; i < 20; i++){
            Counter = Integer.parseInt(driver.findElement(By.xpath("//span[text()[contains(.,'" + Constants.Chapter + "')]]/../../"))
                    .getAttribute("id").replace("p",""));
            Counter++;
            System.out.println(driver.findElement(By.xpath("//div[@id='p"+Counter+"']/span/span")).getText());
            if (driver.findElement(By.xpath("/div/span/span[text()[contains(.,'" + Constants.Chapter + "')]]/../../"))
                    .getText().contains(NextChapter)) break;
        }
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
