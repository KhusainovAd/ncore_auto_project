package consultant_test;

import org.openqa.selenium.WebDriver;

import static consultant_test.Wait.*;

/**
 * Created by User on 07.06.2018.
 */
public class Timer {
    public static double GetTime(WebDriver driver, String Xpath, int millis) throws Exception {
        long start = System.currentTimeMillis();
        Wait.until(driver, Xpath, millis);
        long finish = System.currentTimeMillis();
        long totalTime = finish - start;
        return totalTime;
    }
}
