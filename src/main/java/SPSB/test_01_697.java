//���� 01-697 ���������� � ������� ���� ������� ������������

package SPSB;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_697 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String code;
    private String name;
    private String ac;
    private static String fileName = "C://Users/AHusainov/Test_data2/spsb.txt";
    private String code_sec;
    private String name_sec;
    //�����-������ ��� �����������
    //private static String login = "ahusainov2";
    //private static String password = "Nerfthis9";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        Path filePath = new File("C://Users/AHusainov/Test_data2/spsb.txt").toPath();
        Charset charset = Charset.defaultCharset();
        List<String> stringList = Files.readAllLines(filePath, charset);
        String[] stringArrayInput = stringList.toArray(new String[]{});
        baseUrl = stringArrayInput[0]; // URL
        login = stringArrayInput[1]; // �����
        password = stringArrayInput[2]; //������
        code = stringArrayInput[3];
        name = stringArrayInput[4];
        ac = stringArrayInput[5];
        code_sec = stringArrayInput[6];
        name_sec = stringArrayInput[7];


        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        driver.findElement(By.xpath("//input")).clear();
        driver.findElement(By.xpath("//input")).sendKeys(login);
        driver.findElement(By.xpath("//div[2]/div/div/input")).clear();
        driver.findElement(By.xpath("//div[2]/div/div/input")).sendKeys(password);
        driver.findElement(By.xpath("//button")).click();
        try {
            Thread.sleep(1000);
            driver.findElement(By.xpath("//form[@id='mainPage-view']/div[2]/div/a")).click();
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        // #2 ������� � ���� ������� ������������
        driver.findElement(By.xpath("//*[@id='menu-det-list']/a/span")).click();

        //����� �� ��������
        driver.findElement(By.xpath("//*[@id='detCodePart']")).clear();
        driver.findElement(By.xpath("//*[@id='detNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='detNamePart']")).sendKeys(name_sec);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //������ ��������������
        driver.findElement(By.xpath("//*[text()='" + code_sec + "']/../td[6]/a[1]/span")).click();

        // #3 ������ �� ������ �������
        driver.findElement(By.xpath("//*[text()='�������']")).click();

        // ��������� ����� �������
        //��������� ����� �� ����
        driver.findElement(By.xpath("//*[@id='spCodePart']")).clear();
        driver.findElement(By.xpath("//*[@id='spNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='spCodePart']")).sendKeys(code);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        try {
            Thread.sleep(2000);
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        //���-����
        driver.findElement(By.xpath("//*[text()='" + code + "']/..//*[@id='cbSelectItem']")).click();

        //������ ��������� �����
        driver.findElement(By.xpath("//*[text()='��������� �����']")).click();

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
