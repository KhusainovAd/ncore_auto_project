//���� 01-711 �������������� ��������������� �������� ������� �����������

package SPSB;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_711 {
    private WebDriver driver;
    private String spsbAdm;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String name_dea;
    private String code_dea;
    //�����-������ ��� �����������
    //private static String login = "ahusainov2";
    //private static String password = "Nerfthis9";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        spsbAdm = ExcelUtils.getCellData(9, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_SPSB);
        code_dea = ExcelUtils.getCellData(1, 7);
        name_dea = ExcelUtils.getCellData(1, 8);

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(spsbAdm);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);
        // #2 ������� � ���� �����������
        Wait.until(driver,"//*[@id='menu-dea-list']/a/span");
        driver.findElement(By.xpath("//*[@id='menu-dea-list']/a/span")).click();

        //����� �� ��������
        //Wait.until(driver, "//*[@id='deaCodePart']");
        //driver.findElement(By.xpath("//*[@id='deaCodePart']")).clear();
        Thread.sleep(3500);
        driver.findElement(By.xpath("//*[@id='deaNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='deaNamePart']")).sendKeys(name_dea);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //������ ��������������
        Thread.sleep(3500);
        driver.findElement(By.xpath("//*[text()='" + code_dea + "']/../td[7]/a[1]/span")).click();

        //������ �������� ������ � ������ ��������� � �������
        int random1 = new Random().nextInt(100);
        name_dea = "name_dea" + random1;

        Wait.until(driver, "//*[@id='input-name']");
        driver.findElement(By.xpath("//*[@id='input-name']")).clear();
        driver.findElement(By.xpath("//*[@id='input-name']")).sendKeys(name_dea);

        Wait.until(driver,"//*[@id='input-value-type']");
        driver.findElement(By.xpath("//*[@id='input-value-type']")).click();
        driver.findElement(By.xpath("//*[@id='input-value-type']/option[2]")).click();

        Wait.until(driver, "//*[text()='��������� � �������']",5000);
        driver.findElement(By.xpath("//*[text()='��������� � �������']")).click();
        driver.findElement(By.xpath("//*[text()='��������� � �������']")).click();

        //������� �����

        //#5 ��������� ������� �� ������� ������������������ ���� ���������� ������� ������������
        //����� �� ����
        Wait.until(driver, "//*[@id='deaNamePart']");
        driver.findElement(By.xpath("//*[@id='deaNamePart']")).clear();
        Wait.until(driver, "//*[@id='deaCodePart']");
        driver.findElement(By.xpath("//*[@id='deaCodePart']")).clear();
        driver.findElement(By.xpath("//*[@id='deaCodePart']")).sendKeys(code_dea);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //��������� �� ���� ������� ������ ������� � ������ ������, ������� ��� �� �������������� � ���������, ��� ������ ��������� � ����������
        driver.findElement(By.xpath("//*[text()='" + name_dea + "']")).isDisplayed();

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
