//���� 01-691 �������� �������

package SPSB;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.apache.commons.logging.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class test_01_692 {
    private WebDriver driver;
    private String spsbAdm;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String code;
    private String name;


    //�����-������ ��� �����������
    //private static String login = "ahusainov2";
    //private static String password = "Nerfthis9";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        spsbAdm = ExcelUtils.getCellData(9, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_SPSB);
        code = ExcelUtils.getCellData(1, 0);
        name = ExcelUtils.getCellData(1, 1);

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(spsbAdm);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);

        // #2 ������� � ������ �������
        Wait.until(driver,"//*[@id='menu-sp-list']/a/span");
        driver.findElement(By.xpath("//*[@id='menu-sp-list']/a/span")).click();

        // #3 	������ � ���� ��� ��� ������������� ������� � ������ ������ ���������

        //����� �� ����
        Wait.until(driver, ".//*[@id='spNamePart']");
        driver.findElement(By.xpath(".//*[@id='spNamePart']")).clear();
        Wait.until(driver,".//*[@id='spCodePart']");
        driver.findElement(By.xpath(".//*[@id='spCodePart']")).clear();
        driver.findElement(By.xpath(".//*[@id='spCodePart']")).sendKeys(code);
        Wait.until(driver, "//*[text()='���������']");
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        Wait.until(driver, "//*[text()='" + code + "']",1500);
        //������������ ��������
        assertTrue(driver.findElement(By.xpath("//*[text()='" + code + "']")).isDisplayed());

        Wait.until(driver,"//*[text()='" + code + "']/../td[6]/a[2]/span");
        driver.findElement(By.xpath("//*[text()='" + code + "']/../td[6]/a[2]/span")).click();
        //������ ����������
        Wait.until(driver, "//*[text()='����������']");
        driver.findElement(By.xpath("//*[text()='����������']")).click();

        //����� �� ��������
        Wait.until(driver,"//*[@id='spCodePart']",3500);
        driver.findElement(By.xpath("//*[@id='spCodePart']")).clear();
        Wait.until(driver, "//*[@id='spNamePart']");
        driver.findElement(By.xpath("//*[@id='spNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='spNamePart']")).sendKeys(name);
        Wait.until(driver,"//*[text()='���������']");
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //�������� ��� ��� ���� � ������
        assertFalse(driver.findElement(By.xpath("//*[text()='" + code + "']")).isDisplayed());


    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
