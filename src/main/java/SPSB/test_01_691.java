//���� 01-691 �������������� �������

package SPSB;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class test_01_691 {
    private WebDriver driver;
    private String spsbAdm;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String code;
    private String name;
    private String ac;

    //�����-������ ��� �����������
    //private static String login = "ahusainov2";
    //private static String password = "Nerfthis9";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        spsbAdm = ExcelUtils.getCellData(9, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_SPSB);
        code = ExcelUtils.getCellData(1, 0);
        name = ExcelUtils.getCellData(1, 1);
        ac = ExcelUtils.getCellData(1, 2);

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(spsbAdm);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);

        // #2 ������� � ������ �������
        Wait.until(driver, "//*[text()=('�������')]");
        driver.findElement(By.xpath("//*[text()=('�������')]")).click();

        // #3 	������ ������ �������������� � ������ �� ��������� ��������
        //����� �� ��������
        Wait.until(driver,"//*[@id='spCodePart']",1500);
        driver.findElement(By.xpath("//*[@id='spCodePart']")).clear();
        driver.findElement(By.xpath("//*[@id='spNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='spNamePart']")).sendKeys(name);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //������ ��������������
        Wait.until(driver, "//*[text()='" + code + "']/../td[6]/a[1]/span");
        driver.findElement(By.xpath("//*[text()='" + code + "']/../td[6]/a[1]/span")).click();

        //�������� ������������� �����
        //��� ������� (���� ����������)
        Wait.until(driver,".//*[@id='input-code']");
        assertEquals(driver.findElement(By.xpath(".//*[@id='input-code']")).getAttribute("disabled"), "true");
        //���������� ��� �������������� ������� (���� ����������)
        Wait.until(driver, ".//*[@id='input-mac2']");
        assertEquals(driver.findElement(By.xpath(".//*[@id='input-mac2']")).getAttribute("disabled"), "true");
        //���������� ��� �������������� ������� (���� ����������)
        Wait.until(driver, ".//*[@id='input-mac2']");
        assertEquals(driver.findElement(By.xpath(".//*[@id='input-create-time']")).getAttribute("disabled"), "true");

        // #5 �������� ������ ������� � ������ ������ ��������� � �������
        int random1 = new Random().nextInt(100000);
        name = name + random1;
        ac = ac + random1;
        Wait.until(driver, "//*[@id='input-name']");
        driver.findElement(By.xpath("//*[@id='input-name']")).clear();
        driver.findElement(By.xpath("//*[@id='input-name']")).sendKeys(name);
        Wait.until(driver, "//*[@id='input-mac1']");
        driver.findElement(By.xpath("//*[@id='input-mac1']")).clear();
        driver.findElement(By.xpath("//*[@id='input-mac1']")).sendKeys(ac);

        Wait.until(driver, "//*[text()='��������� � �������']");
        driver.findElement(By.xpath("//*[text()='��������� � �������']")).click();

        //������
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_SPSB);
        ExcelUtils.setCellData(code, 1, 0);
        ExcelUtils.setCellData(name, 1, 1);
        ExcelUtils.setCellData(ac, 1, 2);

        //����� �� ��������
        Wait.until(driver, "//*[@id='spNamePart']");
        driver.findElement(By.xpath("//*[@id='spNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='spNamePart']")).sendKeys(name);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //��������� �� ���� ������� ������ ������� � ������ ������, ������� ��� �� �������������� � ���������, ��� ������ ��������� � ����������
        driver.findElement(By.xpath("//*[text()='" + code + "']")).isDisplayed();

        //������ ��������������
        driver.findElement(By.xpath("//*[text()='" + code + "']/../td[6]/a[1]/span")).click();

        Wait.until(driver,"//*[@id='input-code']");
        //��������
        assertEquals(code, driver.findElement(By.xpath("//*[@id='input-code']")).getAttribute("value"));
        assertEquals(name, driver.findElement(By.xpath("//*[@id='input-name']")).getAttribute("value"));
        //assertEquals (ac,  driver.findElement(By.xpath("//*[@id='input-mac1']")).getAttribute("value"));

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
