//���� 01-702 ���������� ������ ����� ������� ������������

package SPSB;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class test_01_702 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String code_sec;
    private String name_sec;
    private String spsbAdm;
    //�����-������ ��� �����������
    //private static String login = "ahusainov2";
    //private static String password = "Nerfthis9";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        spsbAdm = ExcelUtils.getCellData(9, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_SPSB);
        code_sec = ExcelUtils.getCellData(1, 3);
        name_sec = ExcelUtils.getCellData(1, 4);


        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(spsbAdm);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);

        // #2 ������� � ���� ������� ������������
        driver.findElement(By.xpath("//*[@id='menu-det-list']/a/span")).click();

        //����� �� ��������
        Wait.until(driver,"//*[@id='detCodePart']");
        driver.findElement(By.xpath("//*[@id='detCodePart']")).clear();
        Wait.until(driver,"//*[@id='detNamePart']");
        driver.findElement(By.xpath("//*[@id='detNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='detNamePart']")).sendKeys(name_sec);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //�������� ��� ���� ��� � ������
        assertTrue(driver.findElement(By.xpath("//*[text()='" + code_sec + "']")).isDisplayed());

        //����� �� ��������
        Wait.until(driver,"//*[@id='detNamePart']",3500);
        driver.findElement(By.xpath("//*[@id='detNamePart']")).clear();
        Wait.until(driver, "//*[@id='detCodePart']");
        driver.findElement(By.xpath("//*[@id='detCodePart']")).clear();
        driver.findElement(By.xpath("//*[@id='detCodePart']")).sendKeys(code_sec);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //�������� ��� ���� ��� � ������
        assertTrue(driver.findElement(By.xpath("//*[text()='" + name_sec + "']")).isDisplayed());

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
