//���� 01-708 ���������� ������ ����� �����������

package SPSB;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class test_01_708 {
    private WebDriver driver;
    private String spsbAdm  ;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String code_der;
    private String name_der;
    //�����-������ ��� �����������
    //private static String login = "ahusainov2";
    //private static String password = "Nerfthis9";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        spsbAdm = ExcelUtils.getCellData(9, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������

        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_SPSB);
        code_der = ExcelUtils.getCellData(1, 5);
        name_der = ExcelUtils.getCellData(1, 6);

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(spsbAdm);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);
        // #2 ������� � ������ �������
        Wait.until(driver,"//*[@id='menu-der-list']/a/span");
        driver.findElement(By.xpath("//*[@id='menu-der-list']/a/span")).click();

        //����� �� ��������
        Wait.until(driver, "//*[@id='derCodePart']");
        driver.findElement(By.xpath("//*[@id='derCodePart']")).clear();
        driver.findElement(By.xpath("//*[@id='derNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='derNamePart']")).sendKeys(name_der);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //�������� ��� ���� ��� � ������
        Thread.sleep(3500);
        assertTrue(driver.findElement(By.xpath("//*[text()='" + code_der + "']")).isDisplayed());

        //����� �� ��������
        Wait.until(driver, "//*[@id='derNamePart']",3500);
        driver.findElement(By.xpath("//*[@id='derNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='derCodePart']")).clear();
        driver.findElement(By.xpath("//*[@id='derCodePart']")).sendKeys(code_der);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //�������� ��� ���� ��� � ������
        assertTrue(driver.findElement(By.xpath("//*[text()='" + name_der + "']")).isDisplayed());

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
