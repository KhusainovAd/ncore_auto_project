//���� 01-703 �������� ���� ����������

package SPSB;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_703 {
    private WebDriver driver;
    private String spsbAdm;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String code_der;
    private String name_der;
    //�����-������ ��� �����������
    //private static String login = "ahusainov2";
    //private static String password = "Nerfthis9";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        spsbAdm = ExcelUtils.getCellData(9, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(spsbAdm);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);
        // #2 ������� � ���� �����������
        Wait.until(driver,"//*[@id='menu-der-list']/a/span");
        driver.findElement(By.xpath("//*[@id='menu-der-list']/a/span")).click();

        //������ �� ������ �������� �
        Wait.until(driver, "//*[text()='�������� ���������']");
        driver.findElement(By.xpath("//*[text()='�������� ���������']")).click();

        //������ �������� ������ � ������ ��������� � �������
        int random1 = new Random().nextInt(100000);

        code_der = "code_der" + random1;
        name_der = "name_der" + random1;

        Wait.until(driver, "//*[@id='input-code']");
        driver.findElement(By.xpath("//*[@id='input-code']")).clear();
        driver.findElement(By.xpath("//*[@id='input-code']")).sendKeys(code_der);

        Wait.until(driver, "//*[@id='input-name']");
        driver.findElement(By.xpath("//*[@id='input-name']")).clear();
        driver.findElement(By.xpath("//*[@id='input-name']")).sendKeys(name_der);

        Wait.until(driver, "//*[text()='��������� � �������']",1500);
        driver.findElement(By.xpath("//*[text()='��������� � �������']")).click();

        //����� �� ��������
        Wait.until(driver, "//*[@id='derCodePart']",1500);
        driver.findElement(By.xpath("//*[@id='derCodePart']")).clear();
        Wait.until(driver, "//*[@id='derNamePart']");
        driver.findElement(By.xpath("//*[@id='derNamePart']")).clear();
        driver.findElement(By.xpath("//*[@id='derNamePart']")).sendKeys(name_der);
        driver.findElement(By.xpath("//*[text()='���������']")).click();

        //��������� �� ���� ������� ������ ������� � ������ ������, ������� ��� �� �������������� � ���������, ��� ������ ��������� � ����������
        driver.findElement(By.xpath("//*[text()='" + code_der + "']")).isDisplayed();

        // #3 �������� �� ������� �����
        //������ �� ������ �������� ���������
        Wait.until(driver,"//*[text()='�������� ���������']");
        driver.findElement(By.xpath("//*[text()='�������� ���������']")).click();

        Wait.until(driver, "//*[@id='input-code']");
        driver.findElement(By.xpath("//*[@id='input-code']")).clear();
        driver.findElement(By.xpath("//*[@id='input-code']")).sendKeys("�����������������������");
        driver.findElement(By.xpath("//*[text()='��������� � �������']")).click();

        //�������� ������ �� ������� ����
        driver.findElement(By.xpath("//*[text()='��� ������� ������ �������� �� ��������� ����,����,������� ������������� � ������']")).isDisplayed();
        Wait.until(driver,"//*[@id='menu-der-list']/a/span");
        driver.findElement(By.xpath("//*[@id='menu-der-list']/a/span")).click();

        // #4 �������� �� ��������� ����������
        //������ �� ������ �������� ���������
        Wait.until(driver,"//*[text()='�������� ���������']");
        driver.findElement(By.xpath("//*[text()='�������� ���������']")).click();

        Wait.until(driver, "//*[@id='input-code']");
        driver.findElement(By.xpath("//*[@id='input-code']")).clear();
        driver.findElement(By.xpath("//*[@id='input-code']")).sendKeys(code_der);

        Wait.until(driver, "//*[@id='input-name']");
        driver.findElement(By.xpath("//*[@id='input-name']")).clear();
        driver.findElement(By.xpath("//*[@id='input-name']")).sendKeys(name_der);

        Wait.until(driver, "//*[text()='��������� � �������']");
        driver.findElement(By.xpath("//*[text()='��������� � �������']")).click();

        driver.findElement(By.xpath("//*[text()='��� ���������� ������� ������������ � ������ ����� ��� ��������������� � ������� ����������������']")).isDisplayed();

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_SPSB);
        ExcelUtils.setCellData(code_der, 1, 5);
        ExcelUtils.setCellData(name_der, 1, 6);


    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
