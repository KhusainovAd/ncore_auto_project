package test_alpha;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ahusainov on 30.03.2018.
 */
public class Philtr {

    public static void main(String[] args) throws IOException {
        Path filePath = new File("C:/Users/ahusainov/Desktop/test/0_20.txt").toPath();
        Charset charset = Charset.defaultCharset();
        List<String> stringList = Files.readAllLines(filePath, charset);
        String[] stringArrayInput = stringList.toArray(new String[] {});
        String String1 = stringArrayInput[0];
        System.out.println(String1);
        ArrayList<Integer> numbers = new ArrayList<>();
                for (String part : String1.split(",")) {
                    numbers.add(Integer.parseInt(part));
                }
        Collections.sort(numbers);
        System.out.println(numbers);
        Collections.reverse(numbers);
        System.out.println(numbers);
    }
}
