package test_alpha;

import java.io.IOException;

/**
 * Created by ahusainov on 30.03.2018.
 */
public class Phact {
    public static void main(String[] args) throws IOException {
        long a = 20;
        long phact=1;
        for (long i = 1; i<=a; i++)
        {
            phact=i*phact;
        }
        System.out.println(phact);
    }
}