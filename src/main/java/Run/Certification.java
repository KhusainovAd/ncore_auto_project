package Run;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

//�����������: ��������: C:\Users\ahusainov\Test_data2\settings_cert.txt
@RunWith(Suite.class)
@Suite.SuiteClasses({

        //Spravochink_UC
        CERT.test_01_452.class,
        CERT.test_01_453.class,
        CERT.test_01_324.class,
        CERT.test_01_326.class,

        //spisok_CRL
        CERT.test_01_80.class,
        CERT.test_01_294.class,
        CERT.test_01_79.class,

        //public_CRL
        CERT.test_01_454.class,
        CERT.test_01_456.class,
        CERT.test_01_455.class,

        //cert.control_group
        CERT.test_01_457.class,
        CERT.test_01_459.class

})
public class Certification {

}