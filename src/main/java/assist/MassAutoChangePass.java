//���� 01_408 ���������� � ���������� ������

package assist;

import assist.Constant;
import assist.ExcelUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

public class MassAutoChangePass {
    private WebDriver driver;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String URL_UDS;

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (���/�����/������)
        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        URL_UDS = "http://rel.sudis.n-core.ru";

        //�� ������ �����:
        for (int i=0; i<122; i++){
            ExcelUtils.setExcelFile(Constant.LoadUsers + Constant.LoadUsersFILE, Constant.LoadUsersPage);
            login = ExcelUtils.getCellData(i, 0); // �����
            password = ExcelUtils.getCellData(i, 1); //������
            try {
                driver.get(URL_UDS);
                Login.login(driver,login,password);
                ChangePass.changepass(driver,password);
                Logout.logout(driver);
            } catch (Exception e) {
                System.out.println();
            }
        }
        ;

    }


    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}