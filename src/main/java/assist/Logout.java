package assist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Logout {

    public static void logout(WebDriver driver) throws Exception {
        Thread.sleep(3000);
        Wait.until(driver, "//span[@class[contains(.,'glyphicon glyphicon-off')]]");
        driver.findElement(By.xpath("//span[@class[contains(.,'glyphicon glyphicon-off')]]")).click();

        Wait.until(driver, "//span[text()[contains(.,'OK')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'OK')]]")).click();

        try {
            Wait.until(driver, "//*[text()[contains(.,'�� ��������������� ��������')]]", 1500);
            driver.findElement(By.xpath("//*[text()[contains(.,'�� ��������������� ��������')]]")).click();
        } catch (Exception e) {}

        //�� ���� 10 ���
        try {
            Wait.until(driver, "http://idp.rel.sudis.n-core.ru/idp/checkAuto");
        } catch (Exception e) {
            System.out.println();
        }
    }
}
