//���� 01-652 �������� ������ ���������� � ������ �������� ������������

package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class test_01_652 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;
    private String Statement;
    private String autolog;
    private String autolog2;
    private String autopass;
    private String autopass2;
    private String autogroup;
    private String autogrpass;
    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        autolog = ExcelUtils.getCellData(1, 0);
        autopass = ExcelUtils.getCellData(1, 1);
        autolog2 = ExcelUtils.getCellData(1, 2);
        autopass2 = ExcelUtils.getCellData(1, 3);
        autogroup = ExcelUtils.getCellData(1, 4);
        autogrpass = ExcelUtils.getCellData(1, 5);


        //String profilePath = "C:\\Users\\ahusainov\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\u87b0lv5.default";
        //FirefoxProfile ffprofile = new FirefoxProfile(new File(profilePath));
        //driver = new FirefoxDriver(ffprofile);

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);
        ////////////////////////////////////////////////////////
        //���������� ���������� (sudis-access-admin)

        //��������� �������� ��������
        Wait.until(driver,"//*[@id='app']/nav/div/div/ul/li[4]/span",2000);
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[4]/span")).click();
        //���� ��� �������
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[3]/div/form/div[1]/div[2]/div/div/div/input")).sendKeys("sudis-access-admin");

        //Edit
        Wait.until(driver,"//*[@id='app']/main/section/div/div[4]/div/div[2]/div[3]/table/tbody/tr/td[5]/div/button[2]",1000 );
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[4]/div/div[2]/div[3]/table/tbody/tr/td[5]/div/button[2]")).click();
        //���������� �������������

        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[6]/div[2]/div/div/button[1]",2500);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[6]/div[2]/div/div/button[1]")).click();
        //"+"

        Wait.until(driver, "//*[@id='app']/main/section/div/div[1]/div[2]/button",1500);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button")).click();

        //�������� ����
        Wait.until(driver, "//*[@id='app']/main/section/div/section/div/div[2]/div[1]/button[1]",1500);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/section/div/div[2]/div[1]/button[1]")).click();
        //*[@id='app']/main/section/div/div[2]/div[1]/button[1]

        //����������� ���-����� (ISOD_SERVICE_ADMIN, ISOD_USER)

        Wait.until(driver, ".//*[@id='app']/main/section/div/section/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/table/tbody/tr[1]/td[1]/div/label/span/span",3000);
        driver.findElement(By.xpath(".//*[@id='app']/main/section/div/section/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/table/tbody/tr[1]/td[1]/div/label/span/span")).click();
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/section/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/table/tbody/tr[2]/td[1]/div/label/span/span")).click();

        //���� � �������
        Wait.until(driver, "//*[@id='app']/main/section/div/section/div/div[2]/div[2]/div/div[3]/div/div[4]/button[1]",1000);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/section/div/div[2]/div[2]/div/div[3]/div/div[4]/button[1]")).click();

        //�������� �������������
        Wait.until(driver,"//*[@id='app']/main/section/div/section/div/div[2]/div[1]/button[2]",1000);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/section/div/div[2]/div[1]/button[2]")).click();

        //���� �� ������
        Wait.until(driver,"//*[@id='app']/main/section/div/section/div/div[2]/div[3]/div/div[2]/div/form/div[1]/div[2]/div/div/div/input");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/section/div/div[2]/div[3]/div/div[2]/div/form/div[1]/div[2]/div/div/div/input")).sendKeys(autolog);

        //���-����
        Wait.until(driver, "//*[@id='app']/main/section/div/section/div/div[2]/div[3]/div/div[3]/div/div[2]/div[3]/table/tbody/tr/td[1]/div/label/span/span",3500);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/section/div/div[2]/div[3]/div/div[3]/div/div[2]/div[3]/table/tbody/tr/td[1]/div/label/span/span")).click();

        //���������-�������
        Wait.until(driver, "//*[@id='app']/main/section/div/section/div/div[2]/div[3]/div/div[3]/div/div[4]/button[1]");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/section/div/div[2]/div[3]/div/div[3]/div/div[4]/button[1]")).click();

        //���������-�������
        Wait.until(driver,"//*[@id='app']/main/section/div/section/div/div[2]/div[5]/button[1]" );
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/section/div/div[2]/div[5]/button[1]")).click();
        try {
            Thread.sleep(7000);
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        //������ ���� � ����� ������

        //�������
        Logout.logout(driver);
        //������

        Login.login(driver,autolog,autopass);
        driver.findElement(By.xpath("//a[text()[contains(.,'������� � ������ �������')]]")).click();

        Wait.until(driver, "//span[text()[contains(.,'�������� ����������')]]",1000);
        driver.findElement(By.xpath("//span[text()[contains(.,'�������� ����������')]]")).click();
        Wait.until(driver,"//div[text()[contains(.,'ISOD_USER')]]",1000);
        //�������� ��� ���������� ������������
        assertTrue(driver.findElement(By.xpath("//div[text()[contains(.,'ISOD_USER')]]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//div[text()[contains(.,'ISOD_SERVICE_ADMIN')]]")).isDisplayed());


    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
