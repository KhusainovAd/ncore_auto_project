//���� 01-32 �������� ������ ��������� �� ����������� ���� �������

package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_32 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;
    private String Statement;
    private String autologin;
    private String autopassword;

    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        autologin = ExcelUtils.getCellData(1, 0);
        autopassword = ExcelUtils.getCellData(1, 1);

        String profilePath = "C:\\Users\\ahusainov\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\u87b0lv5.default";
        FirefoxProfile ffprofile = new FirefoxProfile(new File(profilePath));
        driver = new FirefoxDriver(ffprofile);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        //�� ������ �����:
        driver.manage().window().maximize();

        String domain = baseUrl.substring(baseUrl.indexOf("//") + 2, baseUrl.indexOf("."));
        String conf = domain + ".sudis.n-core.ru/conf";
        String dist = domain + ".sudis.n-core.ru/dist";
        String cert = domain + ".sudis.n-core.ru/cert";
        String oper = domain + ".sudis.n-core.ru/oper";
        String acad = domain + ".sudis.n-core.ru/acad";

        //��������
        driver.get(baseUrl);
        //������

        Login.login(driver, autologin, autopassword);

        //�������� ��� ���� �������� (������������ ������ ������� ������):
        Wait.until(driver, "//h1[text()[contains(.,'������������')]]",3500);
        //�������� ������� ���������
        driver.findElement(By.xpath("//h1[text()[contains(.,'������������')]]")).isDisplayed();

        //�������
        Logout.logout(driver);

        //�������� dist
        driver.get(dist);
        //������

        Login.login(driver, autologin, autopassword);

        //�������� ��� ���� �������� (������������ ������ ������� ������):
        Wait.until(driver, "//h1[text()[contains(.,'������������')]]");

        //�������� ������� ���������
        driver.findElement(By.xpath("//h1[text()[contains(.,'������������')]]")).isDisplayed();
        //�������
        Logout.logout(driver);

        //�������� CERT
        driver.get(cert);

        //������
        Login.login(driver, autologin, autopassword);

        //�������� ��� ���� �������� (������������ ������ ������� ������):
        Wait.until(driver, "//h1[text()[contains(.,'������������')]]");

        //�������� ������� ���������
        driver.findElement(By.xpath("//h1[text()[contains(.,'������������')]]")).isDisplayed();

        //�������
        Logout.logout(driver);

        //�������� dist
        driver.get(oper);

        //������
        Login.login(driver, autologin, autopassword);

        //�������� ��� ���� �������� (������������ ������ ������� ������):
        Wait.until(driver, "//h1[text()[contains(.,'������������')]]");

        //�������� ������� ���������
        driver.findElement(By.xpath("//h1[text()[contains(.,'������������')]]")).isDisplayed();

        //�������
        Logout.logout(driver);

        //�������� acad
        driver.get(acad);
        //������

        Login.login(driver, autologin, autopassword);

        //�������� ��� ���� �������� (������������ ������ ������� ������):
        Wait.until(driver, "//h1[text()[contains(.,'������������')]]");

        //�������� ������� ���������
        driver.findElement(By.xpath("//h1[text()[contains(.,'������������')]]")).isDisplayed();

        //�������
        Logout.logout(driver);

        //�������� conf
        driver.get(conf);
        //������

        Login.login(driver, autologin, autopassword);

        //�������� ��� ���� �������� (������������ ������ ������� ������):

        Wait.until(driver, "//h1[text()[contains(.,'������������')]]");

        //�������� ������� ���������
        driver.findElement(By.xpath("//h1[text()[contains(.,'������������')]]")).isDisplayed();

        //�������
        Logout.logout(driver);

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
