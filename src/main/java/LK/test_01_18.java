package LK;

import assist.Constant;
import assist.ExcelUtils;
import assist.LoginCert;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class test_01_18 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;

    private String autologin;
    private String autopassword;

    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);

        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        String profilePath = "C:\\Users\\ahusainov\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\u87b0lv5.default";
        FirefoxProfile ffprofile = new FirefoxProfile(new File(profilePath));
        driver = new FirefoxDriver(ffprofile);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(IDP_URL);

        driver.manage().window().maximize();

        LoginCert.logincert(driver);

        //�������� ��� ���� �������� (������������ ������ ������� ������):
        Wait.until(driver,"//*[text()[contains(.,'������� ������')]]");
        assertTrue(driver.findElement(By.xpath("//*[text()[contains(.,'������� ������')]]")).isDisplayed());

        //�������
        Wait.until(driver,"//*[text()[contains(.,'������� ������')]]");
        driver.findElement(By.xpath("//a[contains(@title, '�����')]/span")).click();
        Wait.until(driver,"//*[text()[contains(.,'OK')]]");
        driver.findElement(By.xpath("//*[text()[contains(.,'OK')]]")).click();

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
