//���� 01-748 ����� ������ � ��
package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class test_01_748 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;
    private String Statement;
    private String autolog;
    private String autolog2;
    private String autopass;
    private String autopass2;
    private String autogroup;
    private String autogrpass;
    private String autotimed;
    private String autotimedpass;
    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        autolog = ExcelUtils.getCellData(1, 0);
        autopass = ExcelUtils.getCellData(1, 1);
        autolog2 = ExcelUtils.getCellData(1, 2);
        autopass2 = ExcelUtils.getCellData(1, 3);
        autogroup = ExcelUtils.getCellData(1, 4);
        autogrpass = ExcelUtils.getCellData(1, 5);
        autotimed = ExcelUtils.getCellData(1, 6);
        autotimedpass = ExcelUtils.getCellData(1, 7);


        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(IDP_URL);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        //������
        Login.login(driver,autolog,autopass);
        try {
            driver.findElement(By.xpath("//a[text()[contains(.,'������� � ������ �������')]]")).click();
            Thread.sleep(1500);
            // any action
        } catch (Exception e) {
            System.out.println();
            ;
        }
        Wait.until(driver,"//span[text()[contains(.,'������� ������')]]",1500);
        driver.findElement(By.xpath("//span[text()[contains(.,'������� ������')]]")).click();

        driver.findElement(By.xpath("//*[@id='app']/main/section/div[1]/div/div[3]/div/div[2]/div/form/div[1]/div/div/input")).clear();
        driver.findElement(By.xpath("//*[@id='app']/main/section/div[1]/div/div[3]/div/div[2]/div/form/div[1]/div/div/input")).sendKeys(autopass);
        autopass = autopass + "2";
        driver.findElement(By.xpath("//*[@id='app']/main/section/div[1]/div/div[3]/div/div[2]/div/form/div[2]/div/div/input")).clear();
        driver.findElement(By.xpath("//*[@id='app']/main/section/div[1]/div/div[3]/div/div[2]/div/form/div[2]/div/div/input")).sendKeys(autopass);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div[1]/div/div[3]/div/div[2]/div/form/div[3]/div/div[1]/input")).clear();
        driver.findElement(By.xpath("//*[@id='app']/main/section/div[1]/div/div[3]/div/div[2]/div/form/div[3]/div/div[1]/input")).sendKeys(autopass);

        Wait.until(driver, "//span[text()[contains(.,'�������� ������')]]",1000);
        driver.findElement(By.xpath("//span[text()[contains(.,'�������� ������')]]")).click();

        Wait.until(driver,"//span[text()[contains(.,'����������')]]",2000);
        driver.findElement(By.xpath("//span[text()[contains(.,'����������')]]")).click();
        ExcelUtils.setCellData(autopass, 1, 1);
        //����������

        //�������
        Thread.sleep(3500);
        Logout.logout(driver);

        driver.get(IDP_URL);
        //�� ������ �����:
        driver.manage().window().maximize();

        Login.login(driver, autolog, autopass);
        try {
            Thread.sleep(1500);
            driver.findElement(By.xpath("//a[text()[contains(.,'������� � ������ �������')]]")).click();
            // any action
        } catch (Exception e) {
            System.out.println();
            ;
        }
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
            ;
        }
        assertTrue(driver.findElement(By.xpath("//h1[text()[contains(.,'������ ������� ������������')]]")).isDisplayed());


    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
