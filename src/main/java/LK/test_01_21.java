//01-21 ��������� ������ �������������� �� ��

package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class test_01_21 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;

    private String autologin;
    private String autopassword;

    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        autologin = ExcelUtils.getCellData(1, 0);
        autopassword = ExcelUtils.getCellData(1, 1);

        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        //������
        Login.login(driver,login,password);

        //��������� �������
        Wait.until(driver,"//span[text()='������� ���� ��� ������']",5000);
        driver.findElement(By.xpath("//span[text()='������� ���� ��� ������']")).click();
        //driver.findElement(By.xpath("//span[text()[contains(.,'������� ���� ��� ������')]]")).click();

        Wait.until(driver,"//label[text()='��� �������']/following-sibling::div/div/input");
        driver.findElement(By.xpath("//label[text()='��� �������']/following-sibling::div/div/input")).sendKeys("sudis-sp-mvd");

        Wait.until(driver,"//div[text()='sudis-sp-mvd']/../..//button[@title='�������������']",3000);
        driver.findElement(By.xpath("//div[text()='sudis-sp-mvd']/../..//button[@title='�������������']")).click();

        //���� ������ �� ����������� �������
        Wait.until(driver,"//span[text()[contains(.,'���� ������ �� ����������� �������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'���� ������ �� ����������� �������')]]")).click();
        Wait.until(driver,"//span[text()='���������']");
        driver.findElement(By.xpath("//span[text()='���������']")).click();

        //�������� ���-���� �������
        Wait.until(driver,"//div[text()='sudis-sp-mvd']/../..//button[@title='�������������']",1500);
        driver.findElement(By.xpath("//div[text()='sudis-sp-mvd']/../..//button[@title='�������������']")).click();
        String ch_box = driver.findElement(By.xpath("//span[text()[contains(.,'���� ������ �� ����������� �������')]]/preceding-sibling::span[1]")).getAttribute("class");
        assertEquals("el-checkbox__input is-checked", ch_box);

        Logout.logout(driver);

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
