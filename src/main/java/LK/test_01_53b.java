//���� 01-53 ���������� ��������� �� //���� ��� ��������� ��

package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class test_01_53b {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;
    private String Statement;
    private String autolog;
    private String autolog2;
    private String autopass;
    private String autopass2;
    private String autogroup;
    private String autogrpass;
    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        autolog = ExcelUtils.getCellData(1, 0);
        autopass = ExcelUtils.getCellData(1, 1);
        autolog2 = ExcelUtils.getCellData(1, 2);
        autopass2 = ExcelUtils.getCellData(1, 3);
        autogroup = ExcelUtils.getCellData(1, 4);
        autogrpass = ExcelUtils.getCellData(1, 5);

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);
        //�������� ������ ������������
        Wait.until(driver,"//span[text()[contains(.,'������������ ���� ��� ������')]]",3500);
        driver.findElement(By.xpath("//span[text()[contains(.,'������������ ���� ��� ������')]]")).click();
        try {
            Thread.sleep(1000);
            // any action
        } catch (Exception e) {
            System.out.println();
        }
        driver.findElement(By.xpath("//i[@class='el-icon-plus']")).click();

        //������� �������
        Random random = new Random();
        String LastName = "�������" + random.nextInt(10000);
        //������� ���
        Random random2 = new Random();
        String FirstName = "������" + random2.nextInt(10000);
        //������� �����
        Random random3 = new Random();
        String autotimed = "AutoLog" + random3.nextInt(10000);

        Wait.until(driver,"//label[text()[contains(.,'�������')]]/following-sibling::*//input",1500);
        //��������� ���������e
        //������� ������ �������
        driver.findElement(By.xpath("//label[text()[contains(.,'�������')]]/following-sibling::*//input")).sendKeys(LastName);
        driver.findElement(By.xpath("//label[text()[contains(.,'���')]]/following-sibling::*//input")).sendKeys(FirstName);
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::*//input")).sendKeys(autotimed);
        //���� ��������
        driver.findElement(By.xpath("//label[text()[contains(.,'���� ��������')]]/following-sibling::*//input")).click();

        Wait.until(driver,"//td[@class='available today current']/preceding-sibling::td[1]",3500);
        driver.findElement(By.xpath("//td[@class='available today current']/preceding-sibling::td[1]")).click();

        Wait.until(driver,"//label[text()[contains(.,'���� ���������')]]/following-sibling::*//input");
        //���� ��������� (�������)
        driver.findElement(By.xpath("//label[text()[contains(.,'���� ���������')]]/following-sibling::*//input")).click();

        Wait.until(driver,"//td[@class='available today current']",3500);
        driver.findElement(By.xpath("//td[@class='available today current']")).click();

        Wait.until(driver,"//label[text()[contains(.,'������')]]/following-sibling::*//input",2000);
        //����������� ������
        String autotimedpass = new String(driver.findElement(By.xpath("//label[text()[contains(.,'������')]]/following-sibling::*//input")).getAttribute("value"));

        //���������-���������
        driver.findElement(By.xpath("//span[text()[contains(.,'���������')]]")).click();

        //�������
        //������ �����
        Logout.logout(driver);


        //������
        Login.login(driver,autotimed,autotimedpass);
        assertTrue(driver.findElement(By.xpath("//p[text()[contains(.,'������ ������� ������ �������������, ���������� � ��������������')]]")).isDisplayed());

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
