//���� 01-33  ���������� ������: "�������� ����������" �� ����� �����

package LK;

import assist.Constant;
import assist.ExcelUtils;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class test_01_33 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;

    private String autologin;
    private String autopassword;

    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        autologin = ExcelUtils.getCellData(1, 0);
        autopassword = ExcelUtils.getCellData(1, 1);

        String profilePath = "C:\\Users\\ahusainov\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\u87b0lv5.default";
        FirefoxProfile ffprofile = new FirefoxProfile(new File(profilePath));
        driver = new FirefoxDriver(ffprofile);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void test() throws Exception {
        driver.get(IDP_URL);
        //�� ������ �����:
        driver.manage().window().maximize();
        Thread.sleep(1500);

        //�����������:
        Wait.until(driver,"//*[text()[contains(.,'����� �� ��')]]",3500);
        driver.findElement(By.xpath("//*[text()[contains(.,'����� �� ��')]]")).click();

        try {
            Wait.until(driver,"//*[text()[contains(.,'�������� ����������')]]");
            assertTrue(driver.findElement(By.xpath("//*[text()[contains(.,'�������� ����������')]]")).isDisplayed());
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        Wait.until(driver,"//*[text()[contains(.,'������ ����������:')]]");
        assertTrue(driver.findElement(By.xpath("//*[text()[contains(.,'������ ����������:')]]")).isDisplayed());

        Wait.until(driver,"//*[text()[contains(.,'�������� ����������')]]/..");
        assertEquals("true", driver.findElement(By.xpath("//*[text()[contains(.,'�������� ����������')]]/..")).getAttribute("disabled"));
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
