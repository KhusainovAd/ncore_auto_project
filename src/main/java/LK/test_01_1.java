//���� 01-1 �������� �������������� � ����� �� �����/������

package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_1 {
    WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    //����� ������ � .txt

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);

        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        String IDP_URL = ExcelUtils.getCellData(1, 4);
        String statement = ExcelUtils.getCellData(1, 5);

        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver, login, password);

        //�������� ������ ������������
        Wait.until(driver,"//*[text()[contains(.,'������������ ���� ��� ������')]]",3500);
        driver.findElement(By.xpath("//*[text()[contains(.,'������������ ���� ��� ������')]]")).click();
        if(driver.findElements(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).size()>0) {
            Wait.until(driver, "//*[@id='app']/main/section/div/div[1]/div[2]/button[1]");
            driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).click();
        }
        else {
            Wait.until(driver, "//a[text()[contains(.,'�������� ������� ������')]]");
            driver.findElement(By.xpath("//a[text()[contains(.,'�������� ������� ������')]]")).click();
        }
        //���-����: ������� ������ ������� (����� �� ���������)
        //driver.findElement(By.xpath("//div[@id='app']/main/section/div/div[2]/div/form/div/div/label/span/span")).click();

        //������� �������
        Random random = new Random();
        String LastName = "�������" + random.nextInt(10000);
        //������� ���
        Random random2 = new Random();
        String FirstName = "������" + random2.nextInt(10000);
        //������� �����
        Random random3 = new Random();
        String autologin = "AutoLog" + random3.nextInt(100000);

        //��������� ����������
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[2]/div/div/input")).sendKeys(LastName);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[3]/div/div/input")).sendKeys(FirstName);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input")).sendKeys("�������");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[9]/div/div/input")).sendKeys(autologin);

        //���� ��������
        driver.findElement(By.xpath("//div[@id='app']/main/section/div/div[2]/div/form/div[5]/div/div/i")).click();
        Wait.until(driver, "html/body//table[1]/tbody//td[@class='available today current']");
        Thread.sleep(100);
        driver.findElement(By.xpath("html/body//table[1]/tbody//td[@class='available today current']")).click();

        //����������� ������
        String autopassword = new String(driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[10]/div/div/div/input")).getAttribute("value"));

        //���������-���������
        driver.findElement(By.xpath(".//*[@id='app']/main/section/div/div[2]/div/form/div[11]/button[1]")).click();

        //�������� ������ ������� ������������
        Wait.until(driver,"//*[text()[contains(.,'������������ " + autologin + " ������')]]");

        Wait.until(driver,"//*[@id='app']/nav/div/div/ul/li[2]/span");
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[2]/span")).click();
        if(driver.findElements(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).size()>0) {
            Wait.until(driver, "//*[@id='app']/main/section/div/div[1]/div[2]/button[1]");
            driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).click();
        }
        else {
            Wait.until(driver, "//a[text()[contains(.,'�������� ������� ������')]]");
            driver.findElement(By.xpath("//a[text()[contains(.,'�������� ������� ������')]]")).click();
        }

        //���-����: ������� ������ ������� (����� �� ���������)
        //driver.findElement(By.xpath("//div[@id='app']/main/section/div/div[2]/div/form/div/div/label/span/span")).click();

        //������� �������
        Random random4 = new Random();
        String LastName2 = "�������" + random4.nextInt(10000);
        //������� ���
        Random random5 = new Random();
        String FirstName2 = "������" + random5.nextInt(10000);
        //������� �����
        Random random6 = new Random();
        String autologin2 = "AutoLog" + random6.nextInt(100000);

        //��������� ����������e
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[2]/div/div/input")).sendKeys(LastName);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[3]/div/div/input")).sendKeys(FirstName);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input")).sendKeys("�������");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[9]/div/div/input")).sendKeys(autologin2);

        //���� ��������
        driver.findElement(By.xpath("//div[@id='app']/main/section/div/div[2]/div/form/div[5]/div/div/i")).click();
        Wait.until(driver, "html/body//table[1]/tbody//td[@class='available today current']");
        Thread.sleep(100);
        driver.findElement(By.xpath("html/body//table[1]/tbody//td[@class='available today current']")).click();

        //����������� ������
        String autopassword2 = new String(driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[10]/div/div/div/input")).getAttribute("value"));

        //���������-���������
        driver.findElement(By.xpath(".//*[@id='app']/main/section/div/div[2]/div/form/div[11]/button[1]")).click();
        Wait.until(driver,"//*[@id='app']/header/div/div/div[2]/ul/li[3]/a/span",100);

        //������ ���� � ����� ������
        //�������
        Wait.until(driver,"//*[text()[contains(.,'������������ " + autologin2 + " ������')]]",1000);

        Logout.logout(driver);

        //������
        Login.login(driver, autologin, autopassword);

        autopassword = ChangePass.changepass(driver, autopassword);

        try {
            Wait.until(driver,"//*[text()[contains(.,'���������� ������')]]");
            driver.findElement(By.xpath("//*[text()[contains(.,'���������� ������')]]")).click();
        } catch (Exception e) {
            System.out.println();
        }

        //�������
        Logout.logout(driver);

        //������ ������ ��
        Login.login(driver, autologin2, autopassword2);

        //������ ������ ������ �� ���-��
        autopassword2 = ChangePass.changepass(driver, autopassword2);

        //������
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        ExcelUtils.setCellData(autologin, 1, 0);
        ExcelUtils.setCellData(autopassword, 1, 1);
        ExcelUtils.setCellData(autologin2, 1, 2);
        ExcelUtils.setCellData(autopassword2, 1, 3);

    }


    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}