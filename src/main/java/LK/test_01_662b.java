// ���� 01-662b �������� ��

package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_662b {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;
    private String Statement;
    private String autologin;
    private String autopassword;
    private String autologin2;
    private String autopassword2;

    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        autologin = ExcelUtils.getCellData(1, 0);
        autopassword = ExcelUtils.getCellData(1, 1);
        autologin2 = ExcelUtils.getCellData(1, 2);
        autopassword2 = ExcelUtils.getCellData(1, 3);

        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        //�� ������ �����:
        driver.manage().window().maximize();

        //�������� logout
        driver.get(baseUrl);

        //������
      Login.login(driver,login,password);
      Wait.until(driver,"//span[text()[contains(.,'������������ ���� ��� ������')]]",3500);
        driver.findElement(By.xpath("//span[text()[contains(.,'������������ ���� ��� ������')]]")).click();

        Wait.until(driver,"//label[text()[contains(.,'�����')]]/following-sibling::div/div/input",1000);
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div/div/input")).clear();
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div/div/input")).sendKeys(autologin2);

        Wait.until(driver,"//div[text()='" + autologin2.toLowerCase() + "']/../../td[6]/div/button[@title='�������������']",1500);
        //������ �������������
        driver.findElement(By.xpath("//div[text()='" + autologin2.toLowerCase() + "']/../../td[6]/div/button[@title='�������������']")).click();

        Wait.until(driver,"//span[text()[contains(.,'��������� ������� ������')]]",1000);
        //������ ��������� ������� ������
        driver.findElement(By.xpath("//span[text()[contains(.,'��������� ������� ������')]]")).click();

        Wait.until(driver,"//div[text()='" + autologin.toLowerCase() + "']/../..//button",1500);
        //������ ������� (������� ����� ��)
        driver.findElement(By.xpath("//div[text()='" + autologin.toLowerCase() + "']/../..//button")).click();

        Wait.until(driver,"//span[text()[contains(.,'�����������')]]",1000);
        //�����������
        driver.findElement(By.xpath("//span[text()[contains(.,'�����������')]]")).click();

        Wait.until(driver,"//a[contains(@title, '�����')]/span",1000);
        //��������� ����� ����������� ���������
        //�������
        Logout.logout(driver);

        //������
        Login.login(driver,login,password);

        driver.findElement(By.xpath("//span[text()[contains(.,'������������ ���� ��� ������')]]")).click();

        Wait.until(driver,"//label[text()[contains(.,'�����')]]/following-sibling::div/div/input",1000);
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div/div/input")).clear();
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div/div/input")).sendKeys(autologin2);

        Wait.until(driver,"//div[text()='" + autologin2.toLowerCase() + "']/../../td[6]/div/button[@title='�������������']",1000);
        //������ �������������;
        driver.findElement(By.xpath("//div[text()='" + autologin2.toLowerCase() + "']/../../td[6]/div/button[@title='�������������']")).click();

        Wait.until(driver, "//span[text()[contains(.,'��������� ������� ������')]]",1000);
        driver.findElement(By.xpath("//span[text()[contains(.,'��������� ������� ������')]]")).click();
        //  driver.findElement(By.xpath("//div[text()[contains(.,'"+autologin.toLowerCase()+"')]]")).getSize() < 1;
        //  assertTrue
        //assertFalse(driver.findElement(By.xpath("//div[text()[contains(.,'"+autologin.toLowerCase()+"')]]")).);

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
