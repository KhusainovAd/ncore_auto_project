//���� 01-60 //���� 01-60 �������� �� ����� �������������. ���� ��� ��������� ��������� ��

package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class test_01_65 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;
    private String Statement;
    private String autolog;
    private String autolog2;
    private String autopass;
    private String autopass2;
    private String autogroup;
    private String autogrpass;
    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_LK);
        autolog = ExcelUtils.getCellData(1, 0);
        autopass = ExcelUtils.getCellData(1, 1);
        autolog2 = ExcelUtils.getCellData(1, 2);
        autopass2 = ExcelUtils.getCellData(1, 3);
        autogroup = ExcelUtils.getCellData(1, 4);
        autogrpass = ExcelUtils.getCellData(1, 5);

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);

        //�������� ������ ������������
        Wait.until(driver,"//span[text()[contains(.,'�������� ������� ������')]]",3500);
        driver.findElement(By.xpath("//span[text()[contains(.,'�������� ������� ������')]]")).click();

        Wait.until(driver,"//label[text()[contains(.,'�����')]]/following-sibling::*//input",2000);
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::*//input")).sendKeys(autogroup);

        Wait.until(driver,"//div[text()[contains(.,'" + autogroup.toLowerCase() + "')]]/../following-sibling::*//button[@title='�������']",10000);
        //������ ��������
        driver.findElement(By.xpath("//div[text()[contains(.,'" + autogroup.toLowerCase() + "')]]/../following-sibling::*//button[@title='�������']")).click();

        Wait.until(driver,"//span[text()[contains(.,'�������')]]",1000);
        //�������
        driver.findElement(By.xpath("//span[text()[contains(.,'�������')]]")).click();

        try {
            Thread.sleep(1500);
            // any action
        } catch (Exception e) {
            System.out.println();
        }
        //�������
        //������ �����
        Logout.logout(driver);

        //������

        Login.login(driver,autogroup,autogrpass);
        assertTrue(driver.findElement(By.xpath("//*[text()[contains(.,'�������� ���� �����/������.')]]")).isDisplayed());


    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
