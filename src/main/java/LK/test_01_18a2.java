//���� 01-18a2 �������/������������ ����������� (� super_user)

//���� 01-18a �������� ����������� (� super_user)

package LK;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_18a2 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String IDP_URL;

    private String autologin;
    private String autopassword;

    //����� ������ � .txt

    private static String fileName = "C://Users/AHusainov/Test_data2/lk.txt";

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);

        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        IDP_URL = ExcelUtils.getCellData(1, 4); //URL idp

        String profilePath = "C:\\Users\\ahusainov\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\u87b0lv5.default";
        FirefoxProfile ffprofile = new FirefoxProfile(new File(profilePath));
        driver = new FirefoxDriver(ffprofile);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(IDP_URL);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver, login,password);

        //System.out.println(driver.findElement(By.xpath(".//*[@id='app']/main/section/div[1]/div/div[1]/div[2]/button[2]/span/span")).getText());
        //System.out.println(driver.findElement(By.xpath("//*[text()[contains(.,'�������� ����������')]]")).getText());
        if (driver.findElements(By.xpath("//*[text()[contains(.,'�������� ����������')]]")).size() > 0) {
            Wait.until(driver,"//*[text()[contains(.,'�������� ����������')]]",500);
            driver.findElement(By.xpath("//*[text()[contains(.,'�������� ����������')]]")).click();
            Wait.until(driver, "//*[text()[contains(.,'�������')]]",500);
            driver.findElement(By.xpath("//*[text()[contains(.,'�������')]]")).click();
            Wait.until(driver, "//*[text()[contains(.,'OK')]]",500);
            driver.findElement(By.xpath("//*[text()[contains(.,'OK')]]")).click();
            Wait.until(driver,"//*[text()='����������']",2000);
            driver.findElement(By.xpath("//*[text()='����������']")).click();
        }
        Wait.until(driver,"//*[text()='��������� ����������']",500);
        driver.findElement(By.xpath("//*[text()='��������� ����������']")).click();
        Wait.until(driver,"//*[text()[contains(.,'�������� ����������')]]",500);
        driver.findElement(By.xpath("//*[text()[contains(.,'�������� ����������')]]")).click();
        Wait.until(driver,"//*[text()[contains(.,'���������')]]",500);
        driver.findElement(By.xpath("//*[text()[contains(.,'���������')]]")).click();

        Thread.sleep(15000);
        //����� ���� ������ ����������� ������ 12345678
        Keyboard keyboard = new Keyboard();
        keyboard.type("12345678");

        Robot r = new Robot();
        try {
            Thread.sleep(500);
            // any action
        } catch (Exception e) {
            System.out.println();
        }
        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        r.keyPress(KeyEvent.VK_TAB);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_TAB);
        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        Wait.until(driver,"//*[text()='����������']",3500);
        driver.findElement(By.xpath("//*[text()='����������']")).click();

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
