package ACAD;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_483 {
    private WebDriver driver;
    private String AccessAdminURL;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String autologin;
    private String autopassword;
    private String GroupLogin;
    private String GroupPassword;

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        AccessAdminURL = ExcelUtils.getCellData(7, 4);
        // ����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_UDS);
        autologin = ExcelUtils.getCellData(1, 0); // �����
        autopassword = ExcelUtils.getCellData(1, 1); //������
        GroupLogin = ExcelUtils.getCellData(1, 6); //��������� �����
        GroupPassword = ExcelUtils.getCellData(1, 7); //��������� ������

        driver = new FirefoxDriver();
        // baseUrl = "http://adm.rel.sudis.n-core.ru/access-admin/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(AccessAdminURL);
        // �� ������ �����:
        driver.manage().window().maximize();
        // �����������:
        Login.login(driver, autologin, autopassword);
        //������� �� "��������� ������� ������"
        Wait.until(driver, "//a[text()[contains(.,'��������� ������� ������')]]", 3500);
        driver.findElement(By.xpath("//a[text()[contains(.,'��������� ������� ������')]]")).click();

        //���� ����� ��. ������
        Wait.until(driver, "//label[text()[contains(.,'�����')]]/following-sibling::div//input");
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div//input")).clear();
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div//input")).sendKeys(GroupLogin);
        //������ �������
        driver.findElement(By.xpath("//span[text()[contains(.,'�������')]]")).click();

        //��������� ���� ��
        Wait.until(driver, "//*[@href[contains(.,'" + GroupLogin.toLowerCase() + "')]]", 1000);
        driver.findElement(By.xpath("//*[@href[contains(.,'" + GroupLogin.toLowerCase() + "')]]")).click();
        //����� ������
        driver.findElement(By.xpath("//span[text()[contains(.,'�������� ������')]]")).click();

        //������� ������
        Wait.until(driver, "//label[text()[contains(.,'������')]]/following-sibling::div//input");
        GroupPassword = new String(driver.findElement(By.xpath("//label[text()[contains(.,'������')]]/following-sibling::div//input")).getAttribute("value"));
        ExcelUtils.setCellData(GroupPassword, 1, 7); //��������� ������

        //������ ���������
        driver.findElement(By.xpath("//span[text()[contains(.,'���������')]]")).click();

        Wait.until(driver, "//*[text()[contains(.,'������������ ������� ��������')]]");
        //�������
        Logout.logout(driver);

        //������
        Login.login(driver, GroupLogin, GroupPassword);

        //������ ������ ������ �� ���-��
        GroupPassword = ChangePass.changepass(driver, GroupPassword);
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_UDS);

        try {
            Wait.until(driver, "//*[text()[contains(.,'���������� ������')]]");
            driver.findElement(By.xpath("//*[text()[contains(.,'���������� ������')]]")).click();
        } catch (Exception e) {
            System.out.println();
        }

        //�������
        Logout.logout(driver);

        // �����������:
        Login.login(driver, autologin, autopassword);

        //������� �� "��������� ������� ������"
        Wait.until(driver, "//a[text()[contains(.,'��������� ������� ������')]]", 3500);
        driver.findElement(By.xpath("//a[text()[contains(.,'��������� ������� ������')]]")).click();

        Wait.until(driver, "//a[text()[contains(.,'��������� ������� ������')]]", 3500);
        driver.findElement(By.xpath("//a[text()[contains(.,'��������� ������� ������')]]")).click();

        //��������� �� .
        if (driver.findElements(By.xpath("//span[@class='el-checkbox__input is-checked']")).size() > 0) {
            Wait.until(driver, "//span[@class='el-checkbox__inner']");
            driver.findElement(By.xpath("//span[@class='el-checkbox__inner']")).click();
        }

        //������ ���������
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[3]/div/div/button[1]")).click();


        //�������
        Logout.logout(driver);

        //������
        Login.login(driver, GroupLogin, GroupPassword);
        driver.getPageSource().contains("������ ������� ������ �������������, ���������� � ��������������");
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}