package ACAD;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class users_01_483 {
    //����� ������ � .txt
    private static String fileName = "C://Users/AHusainov/Test_data2/settings_access_admin.txt";
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String Statement;

    @Before
    public void setUp() throws Exception {

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        Statement = ExcelUtils.getCellData(1, 5);

        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        //�����������:
        Login.login(driver,login,password);

        //��������� �� �������: "��������� ��. ������"
        Wait.until(driver, "//span[text()[contains(.,'�������� ������� ������')]]",3000);
        driver.findElement(By.xpath("//span[text()[contains(.,'�������� ������� ������')]]")).click();
        //�������� ����� ����. ��. ������
        Wait.until(driver, "//*[@class='el-icon-plus']", 1500);
        driver.findElement(By.xpath("//*[@class='el-icon-plus']")).click();

        //������� ��� ������
        Random random = new Random();
        String groupName = "���������" + random.nextInt(10000);
        //������� �������� ������
        Random random2 = new Random();
        String descritpion = "��������" + random2.nextInt(10000);
        //������� �����
        Random random3 = new Random();
        String GroupLogin = "GroupLogin" + random3.nextInt(10000);
        String supervisor = login;

        //��������� ����������
        Wait.until(driver,"//label[text()[contains(.,'��������')]]/following-sibling::div//input");
        driver.findElement(By.xpath("//label[text()[contains(.,'��������')]]/following-sibling::div//input")).sendKeys(groupName);
        Wait.until(driver,"//label[text()[contains(.,'��������')]]/following-sibling::div//input");
        driver.findElement(By.xpath("//label[text()[contains(.,'��������')]]/following-sibling::div//input")).sendKeys(descritpion);
        Wait.until(driver,"//label[text()[contains(.,'�������������')]]/following-sibling::div//input");
        driver.findElement(By.xpath("//label[text()[contains(.,'�������������')]]/following-sibling::div//input")).sendKeys(supervisor);
        Wait.until(driver,"//label[text()[contains(.,'����� ������')]]/following-sibling::div//input");
        driver.findElement(By.xpath("//label[text()[contains(.,'����� ������')]]/following-sibling::div//input")).sendKeys(GroupLogin);

        //���-����
        /*
        if(driver.findElements(By.xpath("//span[@class='el-checkbox__input']")).size()>0) {
            Wait.until(driver, "//span[@class='el-checkbox__inner']");
            driver.findElement(By.xpath("//span[@class='el-checkbox__inner']")).click();
        }
        */

        //������ �������������
        Wait.until(driver,"//label[text()[contains(.,'�������������')]]/following-sibling::div//input");
        driver.findElement(By.xpath("//label[text()[contains(.,'�������������')]]/following-sibling::div//input")).sendKeys(Statement);
        Wait.until(driver, "//li[1][text()[contains(.,'"+Statement+"')]]",1000);
        driver.findElement(By.xpath("//li[1][text()[contains(.,'"+Statement+"')]]")).click();

        //����������� ������
        String GroupPassword = new String(driver.findElement(By.xpath("//label[text()[contains(.,'������')]]/following-sibling::div//input")).getAttribute("value"));

        //��������� � �������
        Wait.until(driver, "//span[text()[contains(.,'���������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'���������')]]")).click();

        //�������
        try {
            Wait.until(driver, "//*[text()[contains(.,'���������� ������')]]");
            driver.findElement(By.xpath("//*[text()[contains(.,'���������� ������')]]")).click();
        } catch (Exception e) {
            System.out.println();
        }

        Logout.logout(driver);

        //������
        Login.login(driver,GroupLogin,GroupPassword);

        //������ ������ ������ �� ���-��
        GroupPassword = ChangePass.changepass(driver,GroupPassword);
        /*
        try {

        } catch (Exception e) {
            System.out.println();
        }*/

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_UDS);
        ExcelUtils.setCellData(GroupLogin, 1, 6);
        ExcelUtils.setCellData(GroupPassword, 1, 7);

    }


    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}