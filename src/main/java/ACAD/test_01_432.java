package ACAD;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_432 {
    private WebDriver driver;
    private String AccessAdminURL;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String Statement;
    private String FIO;
    private String autologin;
    private String autopassword;
    private String autologin2;
    private String autopassword2;

    @Before
    public void setUp() throws Exception {

        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        AccessAdminURL = ExcelUtils.getCellData(7, 4);
        Statement = ExcelUtils.getCellData(1, 5);
        // ����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_UDS);
        FIO = ExcelUtils.getCellData(2, 0) + " " + ExcelUtils.getCellData(2, 1);
        autologin = ExcelUtils.getCellData(1, 0); // �����
        autopassword = ExcelUtils.getCellData(1, 1); //������
        autologin2 = ExcelUtils.getCellData(1, 2); // �����
        autopassword2 = ExcelUtils.getCellData(1, 3); //������

        driver = new FirefoxDriver();
        // baseUrl = "http://adm.rel.sudis.n-core.ru/access-admin/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(AccessAdminURL);
        // �� ������ �����:
        driver.manage().window().maximize();
        // �����������:
        Login.login(driver, autologin, autopassword);

        // #1 ���� AutoTestUser01
        Wait.until(driver, "//div[@id='app']//div/input[@placeholder='�����']", 1000);
        driver.findElement(By.xpath("//div[@id='app']//div/input[@placeholder='�����']")).clear();
        driver.findElement(By.xpath("//div[@id='app']//div/input[@placeholder='�����']")).sendKeys(autologin2);
        Wait.until(driver, "//span[text()[contains(.,'�������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'�������')]]")).click();
        // #2 ��������� ���

        Wait.until(driver, "//*[@href[contains(.,'" + autologin2 + "')]]", 3500);
        driver.findElement(By.xpath("//*[@href[contains(.,'" + autologin2 + "')]]")).click();

        //div[@id='app']//*[@href='#/account/autotestuser01']

        // #3 ������ �� ������: "�������� ������"
        Wait.until(driver, "//span[text()[contains(.,'�������� ������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'�������� ������')]]")).click();

        Wait.until(driver, "//label[text()[contains(.,'������')]]/following-sibling::div//input");
        // #4 ����������� ������
        autopassword2 = new String(driver.findElement(By.xpath("//label[text()[contains(.,'������')]]/following-sibling::div//input")).getAttribute("value"));
        Wait.until(driver, "//span[text()[contains(.,'���������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'���������')]]")).click();

        Wait.until(driver, "//*[text()[contains(.,'������������ ������� ��������')]]");
        // �����
        Logout.logout(driver);

        // ����� � ����� �������
        Login.login(driver, autologin2, autopassword2);

        autopassword2 = ChangePass.changepass(driver, autopassword2);

        Wait.until(driver, "//a[text()[contains(.,'" + Statement + "')]]", 1000);
        driver.findElement(By.xpath("//a[text()[contains(.,'" + Statement + "')]]")).click();
        try {
            Wait.until(driver, "//*[text()[contains(.,'���������� ������')]]");
            driver.findElement(By.xpath("//*[text()[contains(.,'���������� ������')]]")).click();
        } catch (Exception e) {
            System.out.println();
        }
        // �����
        Logout.logout(driver);

        // �����������:
        Login.login(driver, autologin, autopassword);
        // #1 ���� AutoTestUser01
        Wait.until(driver, "//div[@id='app']//div/input[@placeholder='�����']");
        driver.findElement(By.xpath("//div[@id='app']//div/input[@placeholder='�����']")).clear();
        driver.findElement(By.xpath("//div[@id='app']//div/input[@placeholder='�����']")).sendKeys(autologin2);
        driver.findElement(By.xpath("//div[@id='app']//button[1]")).click();

        // #2 ��������� ���
        Wait.until(driver, "//*[@href[contains(.,'" + autologin2 + "')]]", 3500);
        driver.findElement(By.xpath("//*[@href[contains(.,'" + autologin2 + "')]]")).click();

        // #3 ��������� ��������� ������� ������
        Wait.until(driver, "//span[text()[contains(.,'��������� ������� ������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'��������� ������� ������')]]")).click();
        driver.getPageSource().contains("�������");

        // #4 ������������� ��
        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div[2]/div/label/span[1]/span", 1000);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div[2]/div/label/span[1]/span")).click();

        // #5 ������ ������ ���������

        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[3]/div/div/button[1]")).click();

        // �����
        Logout.logout(driver);

        // �� ����� (������� ������ �� �������)
        Login.login(driver, autologin, autopassword);
        driver.getPageSource().contains("������ ������� ������ �������������, ���������� � ��������������");

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_UDS);
        ExcelUtils.setCellData(autopassword, 1, 3);
        ExcelUtils.setCellData(autopassword2, 1, 5);

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}