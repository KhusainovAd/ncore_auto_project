package ACAD;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_431 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String autologin;
    private String autopassword;
    private String statement;
    private String FIO;
    private String AccessAdminURL;
    // ����� ������ � .txt
    // private static String fileName =
    // "C://Users/AHusainov/Test_data/settings_access_admin.txt";
    // �����-������ ��� �����������
    // private static String login = "ahusainov2";
    // private static String password = "Nerfthis9";

    @Before
    public void setUp() throws Exception {

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        AccessAdminURL = ExcelUtils.getCellData(7, 4);
        statement = ExcelUtils.getCellData(1, 5);
        // ����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_UDS);
        autologin = ExcelUtils.getCellData(1, 0); // �����
        autopassword = ExcelUtils.getCellData(1, 1); //������
        FIO = ExcelUtils.getCellData(2, 0) + " " + ExcelUtils.getCellData(2, 1);

        driver = new FirefoxDriver();
        // baseUrl = "http://adm.rel.sudis.n-core.ru/access-admin/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(AccessAdminURL);
        // �� ������ �����:
        driver.manage().window().maximize();
        // �����������:
        Login.login(driver, autologin, autopassword);

        // #1 ��������� � �����������������
        Wait.until(driver, "//a[contains(@href, '#/manager')]", 2000);
        driver.findElement(By.xpath("//a[contains(@href, '#/manager')]")).click();

        // #2 ������ �� ������ "�������� ����������"
        Wait.until(driver, "//span[text()[contains(.,'���������� ����������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'���������� ����������')]]")).click();

        // #3 ������ �� ������ ��������� ������������� � ������� �� ������
        // ������� ���������� ��� ������ �� �. ������. (���� �� ��� ������ �� �.da
        // ������)
        Wait.until(driver, "//span[text()[contains(.,'������� �������������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'������� �������������')]]")).click();
        Wait.until(driver, "//input[@placeholder[contains(.,'�������� �������������...')]]");
        driver.findElement(By.xpath("//input[@placeholder[contains(.,'�������� �������������...')]]")).clear();

        driver.findElement(By.xpath("//input[@placeholder[contains(.,'�������� �������������...')]]")).sendKeys(statement);
        Wait.until(driver, "//tr[1]//div[text()[contains(.,'" + statement + "')]]/../preceding-sibling::td[1]//span", 1000);
        driver.findElement(By.xpath("//tr[1]//div[text()[contains(.,'" + statement + "')]]/../preceding-sibling::td[1]//span")).click();

        Wait.until(driver, "//span[text()[contains(.,'��������')]]",2500);
        driver.findElement(By.xpath("//span[text()[contains(.,'��������')]]")).click();

        // #4 ������ �� ������ ��������� �������������.
        Wait.until(driver, "//span[text()[contains(.,'������� �������������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'������� �������������')]]")).click();

        // #5(1 ����� � ������ ������������ �������� ������������� 5.
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div//input")).clear();
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div//input")).sendKeys(autologin);
        //driver.findElement(By.cssSelector("div.el-form-item__content > button.el-button.el-button--primary")).click();
        Wait.until(driver, "//span[text()[contains(.,'�������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'�������')]]")).click();

        //���-����
        Wait.until(driver, "//tr[1]//div[text()[contains(.,'" + autologin + "')]]/../preceding-sibling::td[1]//span", 1000);
        driver.findElement(By.xpath("//tr[1]//div[text()[contains(.,'"+autologin+"')]]/../preceding-sibling::td[1]//span")).click();

        // #5(2) ������ �� ������ ����������
        Wait.until(driver, "//span[text()[contains(.,'��������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'��������')]]")).click();

        // �6 ������ �� ������ ���������� � ��������.
        Wait.until(driver, "//span[text()[contains(.,'���������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'���������')]]")).click();

        Wait.until(driver, "//*[text()[contains(.,'���������� ������� ���������')]]");

        /*
        driver.get(AccessAdminURL);
        Wait.until(driver, "//a[contains(@href, '#/manager')]",2000);
        driver.findElement(By.xpath("//a[contains(@href, '#/manager')]")).click();

        Wait.until(driver, "//*[@id='app']/main/section/div/div[3]/div/div[3]/div[3]/table/tbody/tr[2]/td[1]/div");
        //Asserts
        assertEquals(autologin.toLowerCase(), driver.findElement(By.xpath(".//*[@id='app']/main/section/div/div[3]/div/div[3]/div[3]/table/tbody/tr[2]/td[1]/div")).getText());
        assertEquals(FIO, driver.findElement(By.xpath(".//*[@id='app']/main/section/div/div[3]/div/div[3]/div[3]/table/tbody/tr[2]/td[2]/div")).getText());
        assertEquals(statement, driver.findElement(By.xpath(".//*[@id='app']/main/section/div/div[3]/div/div[3]/div[3]/table/tbody/tr[2]/td[3]/div")).getText());

        //�����
        driver.findElement(By.xpath("//li[3]/a/span")).click();
        driver.findElement(By.xpath("//body/div[2]/div/div[3]/button[2]")).click();
        */

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}