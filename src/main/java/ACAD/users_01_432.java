package ACAD;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class users_01_432 {
    //����� ������ � .txt
    private static String fileName = "C://Users/AHusainov/Test_data2/settings_access_admin.txt";
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String Statement;

    @Before
    public void setUp() throws Exception {
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(1, 1); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        Statement = ExcelUtils.getCellData(1, 5);

        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl);
        //�� ������ �����:
        driver.manage().window().maximize();

        System.out.println(login);

        //�����������:
        Login.login(driver,login,password);

        //�������� ������ ������������
        Wait.until(driver, "//*[text()[contains(.,'������������ ���� ��� ������')]]", 3500);
        driver.findElement(By.xpath("//*[text()[contains(.,'������������ ���� ��� ������')]]")).click();
        if (driver.findElements(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).size() > 0) {
            Wait.until(driver, "//*[@id='app']/main/section/div/div[1]/div[2]/button[1]");
            driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).click();
        } else {
            Wait.until(driver, "//a[text()[contains(.,'�������� ������� ������')]]");
            driver.findElement(By.xpath("//a[text()[contains(.,'�������� ������� ������')]]")).click();
        }
        //���-����: ������� ������ ������� (����� �� ���������)
        //driver.findElement(By.xpath("//div[@id='app']/main/section/div/div[2]/div/form/div/div/label/span/span")).click();

        //������� �������
        Random random = new Random();
        String LastName = "�������" + random.nextInt(10000);
        //������� ���
        Random random2 = new Random();
        String FirstName = "������" + random2.nextInt(10000);
        //������� �����
        Random random3 = new Random();
        String autologin = ("AutoLog" + random3.nextInt(100000)).toLowerCase();

        //��������� ����������
        driver.findElement(By.xpath("//label[text()[contains(.,'�������')]]/following-sibling::div//input")).sendKeys(LastName);
        driver.findElement(By.xpath("//label[text()[contains(.,'���')]]/following-sibling::div//input")).sendKeys(FirstName);
        driver.findElement(By.xpath("//label[text()[contains(.,'��������')]]/following-sibling::div//input")).sendKeys("�������");
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div//input")).sendKeys(autologin);
        driver.findElement(By.xpath("//label[text()[contains(.,'�������������')]]/following-sibling::div//input")).sendKeys(Statement);
        //����� ������ �������
        Wait.until(driver, "//li[1][text()[contains(.,'"+Statement+"')]]",1000);
        driver.findElement(By.xpath("//li[1][text()[contains(.,'"+Statement+"')]]")).click();

        //���� ��������
        driver.findElement(By.xpath("//div[@id='app']/main/section/div/div[2]/div/form/div[5]/div/div/i")).click();
        Wait.until(driver, "html/body//table[1]/tbody//td[@class='available today current']");
        Thread.sleep(100);
        driver.findElement(By.xpath("html/body//table[1]/tbody//td[@class='available today current']")).click();

        //����������� ������
        String autopassword = new String(driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[10]/div/div/div/input")).getAttribute("value"));

        //���������-���������
        driver.findElement(By.xpath(".//*[@id='app']/main/section/div/div[2]/div/form/div[11]/button[1]")).click();

        //���� ���� ������������ ���������
        Wait.until(driver, "//*[text()[contains(.,'������������ " + autologin + " ������')]]");

        //������� ��� ���� ��
        Wait.until(driver, "//*[text()[contains(.,'������������ ���� ��� ������')]]", 3500);
        driver.findElement(By.xpath("//*[text()[contains(.,'������������ ���� ��� ������')]]")).click();
        if (driver.findElements(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).size() > 0) {
            Wait.until(driver, "//*[@id='app']/main/section/div/div[1]/div[2]/button[1]");
            driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).click();
        } else {
            Wait.until(driver, "//a[text()[contains(.,'�������� ������� ������')]]");
            driver.findElement(By.xpath("//a[text()[contains(.,'�������� ������� ������')]]")).click();
        }
        //���-����: ������� ������ ������� (����� �� ���������)
        //driver.findElement(By.xpath("//div[@id='app']/main/section/div/div[2]/div/form/div/div/label/span/span")).click();

        //������� �������
        Random random1 = new Random();
        String LastName2 = "�������" + random.nextInt(10000);
        //������� ���
        Random random12 = new Random();
        String FirstName2 = "������" + random2.nextInt(10000);
        //������� �����
        Random random13 = new Random();
        String autologin2 = ("AutoLog" + random3.nextInt(100000)).toLowerCase();

        //��������� ����������
        //��������� ����������
        driver.findElement(By.xpath("//label[text()[contains(.,'�������')]]/following-sibling::div//input")).sendKeys(LastName2);
        driver.findElement(By.xpath("//label[text()[contains(.,'���')]]/following-sibling::div//input")).sendKeys(FirstName2);
        driver.findElement(By.xpath("//label[text()[contains(.,'��������')]]/following-sibling::div//input")).sendKeys("�������");
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div//input")).sendKeys(autologin2);
        driver.findElement(By.xpath("//label[text()[contains(.,'�������������')]]/following-sibling::div//input")).sendKeys(Statement);
        //����� ������ �������
        Wait.until(driver, "//li[1][text()[contains(.,'"+Statement+"')]]",1000);
        driver.findElement(By.xpath("//li[1][text()[contains(.,'"+Statement+"')]]")).click();

        //���� ��������
        driver.findElement(By.xpath("//div[@id='app']/main/section/div/div[2]/div/form/div[5]/div/div/i")).click();
        Wait.until(driver, "html/body//table[1]/tbody//td[@class='available today current']");
        Thread.sleep(100);
        driver.findElement(By.xpath("html/body//table[1]/tbody//td[@class='available today current']")).click();

        //����������� ������
        String autopassword2 = new String(driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[10]/div/div/div/input")).getAttribute("value"));

        //���������-���������
        driver.findElement(By.xpath(".//*[@id='app']/main/section/div/div[2]/div/form/div[11]/button[1]")).click();

        //���� ���� ������������ ���������
        Wait.until(driver, "//*[text()[contains(.,'������������ " + autologin2 + " ������')]]");

        Wait.until(driver, "//*[text()[contains(.,'������������ ���� ��� ������')]]", 3500);
        driver.findElement(By.xpath("//*[text()[contains(.,'������������ ���� ��� ������')]]")).click();

        //����������� ��
        //���� ������������
        driver.findElement(By.xpath("//label[text()='�����']/following-sibling::div/div/input")).clear();
        driver.findElement(By.xpath("//label[text()='�����']/following-sibling::div/div/input")).sendKeys(autologin);

        //������ Edit
        Wait.until(driver, "//div[text()='" + autologin.toLowerCase() + "']/../../td[6]/div/button[@title='�������������']", 2500);
        driver.findElement(By.xpath("//div[text()='" + autologin.toLowerCase() + "']/../../td[6]/div/button[@title='�������������']")).click();
        //��������� ��
        Wait.until(driver, "//span[text()[contains(.,'��������� ������� ������')]]",3500);
        driver.findElement(By.xpath("//span[text()[contains(.,'��������� ������� ������')]]")).click();

        //"����" ������
        Wait.until(driver, "//*[@class='el-icon-plus']", 1500);
        driver.findElement(By.xpath("//*[@class='el-icon-plus']")).click();

        //���� �� ������
        driver.findElement(By.xpath("//label[text()='�����']/following-sibling::div/div/input")).clear();
        driver.findElement(By.xpath("//label[text()='�����']/following-sibling::div/div/input")).sendKeys(autologin2);

        //���-����
        Wait.until(driver,"//div[text()[contains(.,'"+autologin2+"')]]/../preceding-sibling::td//span",1500);
        driver.findElement(By.xpath("//div[text()[contains(.,'"+autologin2+"')]]/../preceding-sibling::td//span")).click();

        //������: "��������� ������"
        Wait.until(driver, "//span[text()[contains(.,'��������� ������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'��������� ������')]]")).click();

        //���� �� ��������� �� ������ �������������
        if (driver.findElements(By.xpath("//span[text()[contains(.,'����������')]]")).size() > 0) {
            Wait.until(driver, "//span[text()[contains(.,'����������')]]",1000);
            driver.findElement(By.xpath("//span[text()[contains(.,'����������')]]")).click();
        }
        //driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/section/div[4]/div/button[1]")).click();
        Wait.until(driver, "//*[text()[contains(.,'������������ ������� ��������')]]");

        //���������� ���������� (sudis-access-admin)

        //��������� �������� ��������
        Wait.until(driver, "//span[text()[contains(.,'������� ���� ��� ������')]]",5000);
        driver.findElement(By.xpath("//span[text()[contains(.,'������� ���� ��� ������')]]")).click();
        //���� ��� �������
        Wait.until(driver, "//label[text()='��� �������']/following-sibling::div/div/input");
        driver.findElement(By.xpath("//label[text()='��� �������']/following-sibling::div/div/input")).sendKeys("sudis-access-admin");
        //Edit

        Wait.until(driver, "//div[text()='sudis-access-admin']/../..//button[@title='�������������']", 3000);
        driver.findElement(By.xpath("//div[text()='sudis-access-admin']/../..//button[@title='�������������']")).click();
        //���������� �������������
        Wait.until(driver, "//span[text()[contains(.,'�������������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'�������������')]]")).click();

        //"+"
        Wait.until(driver, "//*[@class='el-icon-plus']", 1500);
        driver.findElement(By.xpath("//*[@class='el-icon-plus']")).click();

        //�������� ����
        Wait.until(driver, "//span[text()[contains(.,'�������� ����')]]", 1500);
        driver.findElement(By.xpath("//span[text()[contains(.,'�������� ����')]]")).click();
        //*[@id='app']/main/section/div/div[2]/div[1]/button[1]

        //����������� ���-����� (ISOD_SERVICE_ADMIN, ISOD_USER) (������� ��� ����� ����� �� ������� � ��� �������� ������ ��� ����� xpath ���������� ����� �� � ����)
        Wait.until(driver, "//div[text()[contains(.,'ISOD_USER')]]/../preceding-sibling::td//span", 1500);
        driver.findElement(By.xpath("//div[text()[contains(.,'ISOD_USER')]]/../preceding-sibling::td//span")).click();
        Wait.until(driver, "//div[text()[contains(.,'ISOD_SERVICE_ADMIN')]]/../preceding-sibling::td//span");
        driver.findElement(By.xpath("//div[text()[contains(.,'ISOD_SERVICE_ADMIN')]]/../preceding-sibling::td//span")).click();

        //���� � �������
        Wait.until(driver, "//span[text()[contains(.,'��������� � �������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'��������� � �������')]]")).click();

        //�������� �������������
        Wait.until(driver, "//span[text()[contains(.,'�������� �������������')]]");
        driver.findElement(By.xpath("//span[text()[contains(.,'�������� �������������')]]")).click();

        //���� �� ������
        Wait.until(driver, "//label[text()[contains(.,'�����')]]/following-sibling::div/div/input");
        driver.findElement(By.xpath("//label[text()[contains(.,'�����')]]/following-sibling::div/div/input")).sendKeys(autologin);


        //���-����
        System.out.println("//div[text()[contains(.,'" + autologin + "')]]/../preceding-sibling::td//span");
        Wait.until(driver, "//div[text()[contains(.,'" + autologin + "')]]/../preceding-sibling::td//span", 4000);
        driver.findElement(By.xpath("//div[text()[contains(.,'" + autologin + "')]]/../preceding-sibling::td//span")).click();

        //���������-�������
        Wait.until(driver, "//span[text()[contains(.,'��������� � �������')]]", 1000);
        driver.findElement(By.xpath("//span[text()[contains(.,'��������� � �������')]]")).click();

        //���������-�������
        Wait.until(driver, "//span[text()[contains(.,'��������� � �������')]]", 1000);
        driver.findElement(By.xpath("//span[text()[contains(.,'��������� � �������')]]")).click();
        //������ ���� � ����� ������


        //�������
        Logout.logout(driver);

        //������
        Login.login(driver,autologin,autopassword);
        //������ ������ ������ �� ���-��
        autopassword = ChangePass.changepass(driver, autopassword);

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_UDS);
        ExcelUtils.setCellData(autologin, 1, 2);
        ExcelUtils.setCellData(autopassword, 1, 3);
        ExcelUtils.setCellData(autologin2, 1, 4);
        ExcelUtils.setCellData(autopassword2, 1, 5);

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}