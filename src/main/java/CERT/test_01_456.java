//���� 01-456 ��������� ������ ���������� ������������

package CERT;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class test_01_456 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;

    @Before
    public void setUp() throws Exception {
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(5, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������

        driver = new FirefoxDriver();
        // baseUrl = "http://adm.rel.sudis.n-core.ru/access-admin/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {


        driver.get(baseUrl);
        driver.manage().window().maximize();
        // �� ������ �����:
        Login.login(driver, login, password);

        // #1 ��������� � ������������� �� ���������� CRL
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[4]/span")).click();
        // #2 ������ ��������������

        Wait.until(driver, "//div[text()='" + login + "']/../..//button", 1000);
        driver.findElement(By.xpath("//div[text()='" + login + "']/../..//button")).click();

        Wait.until(driver, "//*[@id='app']/main/section/div[2]/div/div[2]/div/form/div/div/label/input", 1000);
        // #3 ���� ������
        driver.findElement(By.xpath("//*[@id='app']/main/section/div[2]/div/div[2]/div/form/div/div/label/input")).clear();
        driver.findElement(By.xpath("//*[@id='app']/main/section/div[2]/div/div[2]/div/form/div/div/label/input")).sendKeys("8001000000");
        //������ "��"
        driver.findElement(By.xpath("//*[@id='app']/main/section/div[2]/div/div[3]/span/button[2]")).click();

        //�������� ��������
        assertEquals("(800)100-00-00", driver.findElement(By.xpath("//div[text()='" + login + "']/../..//td[4]//span")).getText());
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}