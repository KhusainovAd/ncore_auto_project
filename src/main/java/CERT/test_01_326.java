//���� 01-326 ��������� ������� ��

package CERT;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import assist.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class test_01_326 {
    //01-453 �������� ����������� ������ � ����������� ������������ ��
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String cer;
    private String adress;

    @Before
    public void setUp() throws Exception {
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(5, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        cer = ExcelUtils.getCellData(5, 5);
        adress = ExcelUtils.getCellData(5, 6);


        driver = new FirefoxDriver();
        // baseUrl = "http://adm.rel.sudis.n-core.ru/access-admin/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {


        driver.get(baseUrl);
        driver.manage().window().maximize();
        // �� ������ �����:
        Login.login(driver, login, password);

        // #1 ��������� � ���������� ��
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[2]/span")).click();

        // #2 ����� � ������� � ������: autotest_00.cer                          ?     +3
        //*[@id='app']/main/section/div/div[4]/div/div[3]/div[3]/table/tbody/tr[1]/td[2]/div
        //*[@id='app']/main/section/div/div[4]/div/div[3]/div[3]/table/tbody/tr[1]/td[5]/div/button/..//button


        // #2 ����� �����������
        Wait.until(driver, "//*[@id='app']/main/section/div/div[3]/div/form/div[2]/div/div/div/div/input");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[3]/div/form/div[2]/div/div/div/div/input")).sendKeys(cer);
        //������ ���������
        Wait.until(driver, "//*[@id='app']/main/section/div/div[3]/div/form/div[3]/div[2]/button[1]");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[3]/div/form/div[3]/div[2]/button[1]")).click();
        //����� � ������ ���� test1.cer.
        //������ �������� ���� �� ������ �������.

        Wait.until(driver, "//*[@id='app']//div[text()='" + cer + "']/../..//button", 10000);
        driver.findElement(By.xpath("//*[@id='app']//div[text()='" + cer + "']/../..//button")).click();

        // #3 ��������: ���������� �������/��������
        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[2]/div/div/input");
        assertEquals(cer, driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[2]/div/div/input")).getAttribute("value"));
        assertEquals("el-checkbox__input is-checked", driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div/label/span[1]")).getAttribute("class"));

        // #4 ����� ������ �� �������
        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div/label/span[1]/span");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div/label/span[1]/span")).click();
        //�������� ����� ����������� OCSP
        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input")).clear();
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input")).sendKeys(adress);
        //������ �� ������ ���������
        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[5]/div/button[1]");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[5]/div/button[1]")).click();

        try {
            Thread.sleep(2000);
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        // #5 ������� ���������� �� �����
        Wait.until(driver, "//*[@id='app']/main/section/div/div[3]/div/form/div[2]/div/div/div/div/input");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[3]/div/form/div[2]/div/div/div/div/input")).clear();
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[3]/div/form/div[2]/div/div/div/div/input")).sendKeys(cer);
        //������ ���������
        Wait.until(driver, "//*[@id='app']/main/section/div/div[3]/div/form/div[3]/div[2]/button[1]");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[3]/div/form/div[3]/div[2]/button[1]")).click();
        //����� � ������ ���� test1.cer.
        //������ �������� ���� �� ������ �������.
        Wait.until(driver, "//*[@id='app']//div[text()='" + cer + "']/../..//button", 1000);

        driver.findElement(By.xpath("//*[@id='app']//div[text()='" + cer + "']/../..//button")).click();

        // #6 ��������: ���������� ��_�������/����� ��������� ���������
        assertEquals("el-checkbox__input", driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div/label/span[1]")).getAttribute("class"));
        assertEquals(adress, driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input")).getAttribute("value"));

        // #7 ���������� ��� �� ������ ����� �����: �� �������/������ ����
        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div/label/span[1]/span");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div/label/span[1]/span")).click();
        //�������� ����� ����������� OCSP
        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input");
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input")).clear();

        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input", 1000);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[4]/div/div/input")).clear();

        Wait.until(driver, "//*[@id='app']/main/section/div/div[2]/div/form/div[5]/div/button[1]", 1000);
        //������ �� ������ ���������
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[5]/div/button[1]")).click();

        //assertEquals(expected, driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div/label/span[1]/span")).getAttribute("value"));

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}