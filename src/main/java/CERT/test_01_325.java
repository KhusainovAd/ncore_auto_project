//���� 01-325 ���������� � ���������� ������ ��

package CERT;

import assist.Constant;
import assist.ExcelUtils;
import assist.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class test_01_325 {
    //01-453 �������� ����������� ������ � ����������� ������������ ��
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String cer;
    private String adress;

    @Before
    public void setUp() throws Exception {
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(5, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        cer = ExcelUtils.getCellData(5, 5);
        adress = ExcelUtils.getCellData(5, 6);

        driver = new FirefoxDriver();
        // baseUrl = "http://adm.rel.sudis.n-core.ru/access-admin/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {


        driver.get(baseUrl);
        driver.manage().window().maximize();
        // �� ������ �����:
        Login.login(driver,login,password);
        // #1 ��������� � ���������� ��
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[2]/span")).click();

        // #2 ������ ���������� �� "�� �������� ��"
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[3]/div/form/div[1]/div/div/div/div/div[1]/input")).click();
        driver.findElement(By.xpath("html/body/div[2]/div/div[1]/ul/li[2]")).click();

        //������ ���������
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[3]/div/form/div[3]/div[2]/button[1]")).click();

        for (int i = 1; i <= 11; i++) {
            WebElement single = driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[4]/div/div[3]/div[3]/table/tbody/tr[" + i + "]/td[2]/div"));
            String str = single.getText() + "\n";
            System.out.print(str);
        }

        //Select dd = new Select(single);


    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}