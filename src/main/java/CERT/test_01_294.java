//���� 01-294 �������� �������� CRL-�����. ����� ��������� �� �������

package CERT;

import assist.Constant;
import assist.ExcelUtils;
import assist.Keyboard;
import assist.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class test_01_294 {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String crl_path;
    private String crl;


    @Before
    public void setUp() throws Exception {
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(5, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        crl_path = ExcelUtils.getCellData(5, 8);

        crl = crl_path.toString().substring(crl_path.lastIndexOf("/") + 1, crl_path.length()); // ��� crl-��

        driver = new FirefoxDriver();
        // baseUrl = "http://adm.rel.sudis.n-core.ru/access-admin/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {

        driver.get(baseUrl);
        driver.manage().window().maximize();
        // �� ������ �����:
        Login.login(driver, login, password);

        // #1 ��������� � ������ CRL
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[3]/span")).click();

        // #2 "+" button
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).click();

        // #3 ������ �� ������ ��������� ������ CRL�
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div[3]/div/div/div/button")).click();

        //��������� ������� �� ��� ������������
        Thread.sleep(1000);
        //C:\Users\ahusainov\Test_data2\<cer>
        Keyboard keyboard = new Keyboard();
        keyboard.type(crl_path);

        Robot r = new Robot();

        try {
            Thread.sleep(1500);
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        try {
            Thread.sleep(3000);
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        //������: "���������"
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[2]/div/div/button[1]")).click();

        try {
            Thread.sleep(1000);
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        //��������
        assertEquals("CRL-���� �������� �����", driver.findElement(By.xpath("//*[@id='app']/div/div/div[2]/div/span")).getText());

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}