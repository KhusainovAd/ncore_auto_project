//���� 01-80 �������� CRL-�����

package CERT;

import assist.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class test_01_80 {
    //01-452 �������� ����������� ��
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String cer;
    private String crl;
    private String crl_path;

    @Before
    public void setUp() throws Exception {
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);
        baseUrl = ExcelUtils.getCellData(5, 4); // URL
        login = ExcelUtils.getCellData(1, 2); // �����
        password = ExcelUtils.getCellData(1, 3); //������
        cer = ExcelUtils.getCellData(5, 5);
        crl_path = ExcelUtils.getCellData(5, 8);// ���� � .crl
        crl = crl_path.toString().substring(crl_path.lastIndexOf("/") + 1, crl_path.length()); // ��� crl-��

        driver = new FirefoxDriver();
        // baseUrl = "http://adm.rel.sudis.n-core.ru/access-admin/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {


        driver.get(baseUrl);
        driver.manage().window().maximize();
        // �� ������ �����:
        Login.login(driver, login, password);
        // #1 ��������� � ������ CRL
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[3]/span")).click();

        // #2 "+" button
        Wait.until(driver, "//*[@id='app']/main/section/div/div[1]/div[2]/button[1]", 3000);
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[1]/div[2]/button[1]")).click();

        // #3 ������ �� ������ ��������� ������ CRL�
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div[3]/div/div/div/button")).click();

        //��������� ������� �� ��� ������������
        Thread.sleep(1000);
        //C:\Users\ahusainov\Test_data2\<cer>
        Keyboard keyboard = new Keyboard();
        keyboard.type(crl_path);

        Robot r = new Robot();

        try {
            Thread.sleep(1500);
            // any action
        } catch (Exception e) {
            System.out.println();
        }
        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        r.keyPress(KeyEvent.VK_ENTER);    // confirm by pressing Enter in the end
        r.keyRelease(KeyEvent.VK_ENTER);
        try {
            Thread.sleep(3000);
            // any action
        } catch (Exception e) {
            System.out.println();
        }

        //������: "���������"
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[2]/div/div/button[1]")).click();

        Wait.until(driver, "//*[@id='app']/nav/div/div/ul/li[4]/span", 1500);
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[4]/span")).click();
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[3]/span")).click();

        //#4 ������������ ����: ������ ��
        //������ �� ������ ��������������
        Wait.until(driver, "//*[@id='app']//div[text()='" + crl + "']/../..//button", 10000);
        driver.findElement(By.xpath("//*[@id='app']//div[text()='" + crl + "']/../..//button")).click();
        //���������� ���-���� ������ ��
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[1]/div/label/span[1]/span")).click();
        //������ �� ������ ���������
        driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[2]/div/form/div[9]/div/button[1]")).click();

        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[4]/span")).click();
        driver.findElement(By.xpath("//*[@id='app']/nav/div/div/ul/li[3]/span")).click();

        //��������
        Wait.until(driver, "//*[@id='app']/main/section/div/div[4]/div/div[3]/div[3]/table/tbody/tr[1]/td[2]/div", 1500);
        assertEquals(crl, driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[4]/div/div[3]/div[3]/table/tbody/tr[1]/td[2]/div")).getText());
        assertEquals("el-icon-check", driver.findElement(By.xpath("//*[@id='app']/main/section/div/div[4]/div/div[3]/div[3]/table/tbody/tr[1]/td[1]/div/i")).getAttribute("class"));

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}