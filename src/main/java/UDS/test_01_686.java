//���� 01_686 ���������� ������ ���������� UDS ��� ���������� ������

package UDS;

import assist.Constant;
import assist.ExcelUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class test_01_686 {
    private WebDriver driver;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private String login;
    private String password;
    private String URL_UDS;
    private String UDS_1;

    @Before
    public void setUp() throws Exception {
        //����������� <a> Web (����/�����/������)
        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Input_data);

        login = ExcelUtils.getCellData(3, 2); // �����
        password = ExcelUtils.getCellData(3, 3); //������
        URL_UDS = ExcelUtils.getCellData(3, 4); //URL Uds


        driver = new FirefoxDriver();
        //baseUrl = "http://web.rel.sudis.n-core.ru/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testUntitled2() throws Exception {
        driver.get(URL_UDS);
        //�� ������ �����:
        driver.manage().window().maximize();
        //�����������:
        driver.findElement(By.xpath("//input")).clear();
        driver.findElement(By.xpath("//input")).sendKeys(login);
        driver.findElement(By.xpath("//div[2]/div/div/input")).clear();
        driver.findElement(By.xpath("//div[2]/div/div/input")).sendKeys(password);
        driver.findElement(By.xpath("//button")).click();
        try {
            Thread.sleep(1000);
            driver.findElement(By.xpath("//form[@id='mainPage-view']/div[2]/div/a")).click();
        } catch (Exception e) {
            System.out.println();
        }
        driver.findElement(By.xpath("//a[text()[contains(.,'���������� �����������')]]")).click();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
        }
        //�������� UDS
        driver.findElement(By.xpath("//a[text()[contains(.,'�������� UDS')]]")).click();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
        }
        driver.findElement(By.xpath("//button[text()[contains(.,'��������')]]")).click();
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
            System.out.println();
        }
        //driver.findElement(By.xpath("//a[text()[contains(.,'���������� ����������� ������')]]")).click();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
        }

        UDS_1 = driver.findElement(By.xpath("//tr[1]/td[1]")).getText();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
        }
        driver.findElement(By.xpath("//td[text()[contains(.,'" + UDS_1 + "')]]/following-sibling::td[*]/a[text()[contains(.,'���������� ����������� ������')]]")).click();
        //������������� � ����� UDS
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
        }
        driver.findElement(By.xpath("//a[text()[contains(.,'������������� � ����� UDS')]]")).click();

        int random = new Random().nextInt(1000);

        driver.findElement(By.xpath(".//*[@id='remoteUdsCode']")).sendKeys(UDS_1 + "_copy_auto" + random);
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println();
        }
        driver.findElement(By.xpath("//a[text()[contains(.,'��������� � �������')]]")).click();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
        }
        //�������� UDS
        driver.findElement(By.xpath("//a[text()[contains(.,'�������� UDS')]]")).click();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
        }
        driver.findElement(By.xpath("//button[text()[contains(.,'��������')]]")).click();
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
            System.out.println();
        }
        //driver.findElement(By.xpath("//a[text()[contains(.,'���������� ����������� ������')]]")).click();
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println();
        }
        assertTrue(driver.findElement(By.xpath("//td[text()[contains(.,'" + UDS_1 + "_copy_auto" + random + "')]]")).isDisplayed());

        ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData, Constant.Data_UDS);

        ExcelUtils.setCellData(UDS_1 + "_copy_auto_" + random, 1, 1); //URL Uds

    }


    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}